/*  1:   */ package gr.ilsp.nlp.fileutils;
/*  2:   */ 
/*  3:   */ import java.io.BufferedReader;
/*  4:   */ import java.io.File;
/*  5:   */ import java.io.FileOutputStream;
/*  6:   */ import java.io.FileReader;
/*  7:   */ import java.io.IOException;
/*  8:   */ 
/*  9:   */ public class FilesUtils
/* 10:   */ {
/* 11:   */   public static String readFile(String filename)
/* 12:   */   {
/* 13:14 */     String content = null;
/* 14:15 */     File file = new File(filename);
/* 15:   */     try
/* 16:   */     {
/* 17:17 */       FileReader reader = new FileReader(file);
/* 18:18 */       char[] chars = new char[(int)file.length()];
/* 19:19 */       reader.read(chars);
/* 20:20 */       content = new String(chars);
/* 21:21 */       reader.close();
/* 22:   */     }
/* 23:   */     catch (IOException e)
/* 24:   */     {
/* 25:23 */       e.printStackTrace();
/* 26:   */     }
/* 27:25 */     return content;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public static String readFileLineByLine(String file)
/* 31:   */     throws IOException
/* 32:   */   {
/* 33:30 */     BufferedReader reader = new BufferedReader(new FileReader(file));
/* 34:31 */     String line = null;
/* 35:32 */     StringBuilder stringBuilder = new StringBuilder();
/* 36:33 */     String ls = System.getProperty("line.separator");
/* 37:35 */     while ((line = reader.readLine()) != null)
/* 38:   */     {
/* 39:37 */       stringBuilder.append(line);
/* 40:38 */       stringBuilder.append(ls);
/* 41:   */     }
/* 42:41 */     return stringBuilder.toString();
/* 43:   */   }
/* 44:   */   
/* 45:   */   public static String readFileLineByLine(String file, String separator)
/* 46:   */     throws IOException
/* 47:   */   {
/* 48:48 */     BufferedReader reader = new BufferedReader(new FileReader(file));
/* 49:49 */     String line = null;
/* 50:50 */     StringBuilder stringBuilder = new StringBuilder();
/* 51:51 */     String sep = separator;
/* 52:53 */     while ((line = reader.readLine()) != null)
/* 53:   */     {
/* 54:55 */       stringBuilder.append(line);
/* 55:56 */       stringBuilder.append(sep);
/* 56:   */     }
/* 57:59 */     return stringBuilder.toString();
/* 58:   */   }
/* 59:   */   
/* 60:   */   public static boolean writeStrToAFile(String str, String file)
/* 61:   */   {
/* 62:   */     try
/* 63:   */     {
/* 64:68 */       FileOutputStream FOS = new FileOutputStream(file);
/* 65:69 */       FOS.write(str.getBytes());
/* 66:70 */       FOS.flush();
/* 67:   */       
/* 68:72 */       return true;
/* 69:   */     }
/* 70:   */     catch (Exception e)
/* 71:   */     {
/* 72:76 */       e.printStackTrace();
/* 73:   */     }
/* 74:77 */     return false;
/* 75:   */   }
/* 76:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.fileutils.FilesUtils
 * JD-Core Version:    0.7.0.1
 */