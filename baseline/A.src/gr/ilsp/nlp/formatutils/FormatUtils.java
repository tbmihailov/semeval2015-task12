/*  1:   */ package gr.ilsp.nlp.formatutils;
/*  2:   */ 
/*  3:   */ import java.text.DecimalFormat;
/*  4:   */ 
/*  5:   */ public class FormatUtils
/*  6:   */ {
/*  7: 7 */   private static DecimalFormat df = new DecimalFormat();
/*  8:   */   
/*  9:   */   public static String appender(String chr, String label, int max)
/* 10:   */   {
/* 11:11 */     StringBuffer sb = new StringBuffer();
/* 12:   */     
/* 13:13 */     sb.append(label);
/* 14:15 */     for (int i = 0; i < max - label.length(); i++) {
/* 15:17 */       sb.append(chr);
/* 16:   */     }
/* 17:20 */     return sb.toString();
/* 18:   */   }
/* 19:   */   
/* 20:   */   public String yield(float n)
/* 21:   */   {
/* 22:26 */     df.setMaximumFractionDigits(4);
/* 23:28 */     if (!Float.isNaN(n)) {
/* 24:30 */       return df.format(n);
/* 25:   */     }
/* 26:34 */     return "NaN";
/* 27:   */   }
/* 28:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.formatutils.FormatUtils
 * JD-Core Version:    0.7.0.1
 */