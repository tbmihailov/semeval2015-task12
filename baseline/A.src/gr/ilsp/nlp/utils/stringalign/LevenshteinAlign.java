/*  1:   */ package gr.ilsp.nlp.utils.stringalign;
/*  2:   */ 
/*  3:   */ import java.io.PrintStream;
/*  4:   */ 
/*  5:   */ public class LevenshteinAlign
/*  6:   */ {
/*  7:   */   public static final String sym = "@";
/*  8:   */   int[][] T;
/*  9:   */   int x;
/* 10:   */   int y;
/* 11:   */   
/* 12:   */   public static void main(String[] args)
/* 13:   */   {
/* 14: 7 */     LevenshteinAlign LA = new LevenshteinAlign();
/* 15:   */     
/* 16: 9 */     String[] aligned = LA.align(" I am OK .", 
/* 17:10 */       " Yes! I am OK .");
/* 18:   */     
/* 19:12 */     System.out.println(aligned[0]);
/* 20:13 */     System.out.println(aligned[1]);
/* 21:   */   }
/* 22:   */   
/* 23:   */   public String[] align(String a, String b)
/* 24:   */   {
/* 25:23 */     this.T = new int[a.length() + 1][b.length() + 1];
/* 26:24 */     this.x = (a.length() + 1);
/* 27:25 */     this.y = (b.length() + 1);
/* 28:27 */     for (int i = 0; i <= a.length(); i++) {
/* 29:28 */       this.T[i][0] = i;
/* 30:   */     }
/* 31:30 */     for (int i = 0; i <= b.length(); i++) {
/* 32:31 */       this.T[0][i] = i;
/* 33:   */     }
/* 34:33 */     for (int i = 1; i <= a.length(); i++) {
/* 35:35 */       for (int j = 1; j <= b.length(); j++) {
/* 36:37 */         if (a.charAt(i - 1) == b.charAt(j - 1)) {
/* 37:38 */           this.T[i][j] = this.T[(i - 1)][(j - 1)];
/* 38:   */         } else {
/* 39:40 */           this.T[i][j] = (Math.min(this.T[(i - 1)][j], this.T[i][(j - 1)]) + 1);
/* 40:   */         }
/* 41:   */       }
/* 42:   */     }
/* 43:44 */     StringBuilder aa = new StringBuilder();StringBuilder bb = new StringBuilder();
/* 44:   */     
/* 45:46 */     int i = a.length();
/* 46:46 */     for (int j = b.length(); (i > 0) || (j > 0);) {
/* 47:48 */       if ((i > 0) && (this.T[i][j] == this.T[(i - 1)][j] + 1))
/* 48:   */       {
/* 49:49 */         aa.append(a.charAt(--i));
/* 50:50 */         bb.append("@");
/* 51:   */       }
/* 52:51 */       else if ((j > 0) && (this.T[i][j] == this.T[i][(j - 1)] + 1))
/* 53:   */       {
/* 54:52 */         bb.append(b.charAt(--j));
/* 55:53 */         aa.append("@");
/* 56:   */       }
/* 57:54 */       else if ((i > 0) && (j > 0) && (this.T[i][j] == this.T[(i - 1)][(j - 1)]))
/* 58:   */       {
/* 59:55 */         aa.append(a.charAt(--i));
/* 60:56 */         bb.append(b.charAt(--j));
/* 61:   */       }
/* 62:   */     }
/* 63:60 */     return new String[] { aa.reverse().toString(), bb.reverse().toString() };
/* 64:   */   }
/* 65:   */   
/* 66:   */   public int getCost()
/* 67:   */   {
/* 68:64 */     return this.T[(this.x - 1)][(this.y - 1)];
/* 69:   */   }
/* 70:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.utils.stringalign.LevenshteinAlign
 * JD-Core Version:    0.7.0.1
 */