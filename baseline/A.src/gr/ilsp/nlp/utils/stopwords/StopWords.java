package gr.ilsp.nlp.utils.stopwords;

import java.io.InputStream;

public abstract interface StopWords
{
  public abstract boolean isStopWord(String paramString);
  
  public abstract void load(InputStream paramInputStream);
}


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.utils.stopwords.StopWords
 * JD-Core Version:    0.7.0.1
 */