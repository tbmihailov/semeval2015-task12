/*  1:   */ package gr.ilsp.nlp.utils.stopwords;
/*  2:   */ 
/*  3:   */ import java.io.BufferedReader;
/*  4:   */ import java.io.InputStream;
/*  5:   */ import java.io.InputStreamReader;
/*  6:   */ import java.util.HashSet;
/*  7:   */ 
/*  8:   */ public class StopWordsFromCSV
/*  9:   */   implements StopWords
/* 10:   */ {
/* 11:   */   private HashSet<String> stpwrds;
/* 12:   */   
/* 13:   */   public void load(InputStream is)
/* 14:   */   {
/* 15:   */     try
/* 16:   */     {
/* 17:18 */       this.stpwrds = new HashSet();
/* 18:19 */       BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
/* 19:20 */       String line = reader.readLine();
/* 20:21 */       String[] words = line.split(",");
/* 21:23 */       for (int i = 0; i < words.length; i++) {
/* 22:25 */         this.stpwrds.add(words[i]);
/* 23:   */       }
/* 24:   */     }
/* 25:   */     catch (Exception e)
/* 26:   */     {
/* 27:30 */       e.printStackTrace();
/* 28:   */     }
/* 29:   */   }
/* 30:   */   
/* 31:   */   public boolean isStopWord(String w)
/* 32:   */   {
/* 33:37 */     return this.stpwrds.contains(w.toLowerCase());
/* 34:   */   }
/* 35:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.utils.stopwords.StopWordsFromCSV
 * JD-Core Version:    0.7.0.1
 */