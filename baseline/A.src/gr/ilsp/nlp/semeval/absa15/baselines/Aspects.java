package gr.ilsp.nlp.semeval.absa15.baselines;

public abstract class Aspects
{
  public abstract String[] getA();
  
  public abstract String[] getB();
}
