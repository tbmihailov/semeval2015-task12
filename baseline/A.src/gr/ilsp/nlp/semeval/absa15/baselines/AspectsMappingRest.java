/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ public class AspectsMappingRest
/*  4:   */   extends Aspects
/*  5:   */ {
/*  6: 6 */   public static final String[] A = {
/*  7: 7 */     "RESTAURANT", "FOOD", "DRINKS", "SERVICE", "AMBIENCE", "LOCATION" };
/*  8:11 */   public static final String[] B = {
/*  9:12 */     "GENERAL", "PRICES", "QUALITY", "STYLE_OPTIONS", "MISCELLANEOUS" };
/* 10:   */   
/* 11:   */   public String[] getA()
/* 12:   */   {
/* 13:17 */     return A;
/* 14:   */   }
/* 15:   */   
/* 16:   */   public String[] getB()
/* 17:   */   {
/* 18:22 */     return B;
/* 19:   */   }
/* 20:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.AspectsMappingRest
 * JD-Core Version:    0.7.0.1
 */