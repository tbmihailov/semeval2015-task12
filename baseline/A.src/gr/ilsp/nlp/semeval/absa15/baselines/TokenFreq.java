/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ public class TokenFreq
/*  4:   */ {
/*  5:   */   private String token;
/*  6:   */   private int f;
/*  7:   */   
/*  8:   */   public TokenFreq(String t, int f)
/*  9:   */   {
/* 10:12 */     this.token = t;
/* 11:13 */     this.f = f;
/* 12:   */   }
/* 13:   */   
/* 14:   */   public String getToken()
/* 15:   */   {
/* 16:18 */     return this.token;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public void setToken(String token)
/* 20:   */   {
/* 21:22 */     this.token = token;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public int getF()
/* 25:   */   {
/* 26:26 */     return this.f;
/* 27:   */   }
/* 28:   */   
/* 29:   */   public void setF(int f)
/* 30:   */   {
/* 31:30 */     this.f = f;
/* 32:   */   }
/* 33:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.TokenFreq
 * JD-Core Version:    0.7.0.1
 */