/*   1:    */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*   2:    */ 
/*   3:    */ import gr.ilsp.nlp.semeval.absa15.eval.ABSA15Eval;
/*   4:    */ import java.io.PrintStream;
/*   5:    */ 
/*   6:    */ public class BaseCmdEmu
/*   7:    */ {
/*   8:    */   public static void main(String[] args)
/*   9:    */   {
/*  10:  9 */     int emu = 1;
/*  11:    */     
/*  12: 11 */     String basedir = "C:/Users/galanisd/Desktop/Dimitris/ILSPDocs/PrjSemEval15Task12WrkShop/Baselines/";
/*  13: 12 */     String ttd = "Files";
/*  14: 13 */     String src = "ABSA-15_Laptops_Train_Data.xml";
/*  15: 14 */     String dom = "lapt";
/*  16: 17 */     if (emu == 0)
/*  17:    */     {
/*  18: 20 */       String[] margs = new String[6];
/*  19:    */       
/*  20: 22 */       margs[0] = "1";
/*  21: 23 */       margs[1] = basedir;
/*  22: 24 */       margs[2] = ttd;
/*  23: 25 */       margs[3] = src;
/*  24: 26 */       margs[4] = "10";
/*  25: 27 */       margs[5] = "9";
/*  26:    */       
/*  27: 29 */       Split.main(margs);
/*  28:    */     }
/*  29: 32 */     if (emu == 1)
/*  30:    */     {
/*  31: 35 */       String[] margs = new String[6];
/*  32:    */       
/*  33: 37 */       margs[0] = "1";
/*  34: 38 */       margs[1] = basedir;
/*  35: 39 */       margs[2] = ttd;
/*  36: 40 */       margs[3] = src;
/*  37: 41 */       margs[4] = "10";
/*  38: 42 */       margs[5] = "9";
/*  39:    */       
/*  40: 44 */       Split.main(margs);
/*  41:    */       
/*  42:    */ 
/*  43: 47 */       margs = new String[4];
/*  44: 48 */       margs[0] = dom;
/*  45: 49 */       margs[1] = basedir;
/*  46: 50 */       margs[2] = ttd;
/*  47: 51 */       margs[3] = "1000";
/*  48:    */       
/*  49: 53 */       ExtractFeats.main(margs);
/*  50:    */       
/*  51:    */ 
/*  52: 56 */       margs = new String[5];
/*  53: 57 */       margs[0] = dom;
/*  54: 58 */       margs[1] = basedir;
/*  55: 59 */       margs[2] = ttd;
/*  56: 60 */       margs[3] = "1";
/*  57: 61 */       margs[4] = "1";
/*  58:    */       
/*  59: 63 */       CreateVecs.main(margs);
/*  60:    */       
/*  61:    */ 
/*  62: 66 */       margs[3] = "2";
/*  63: 67 */       margs[4] = "1";
/*  64: 68 */       CreateVecs.main(margs);
/*  65:    */     }
/*  66: 73 */     if (emu == 2)
/*  67:    */     {
/*  68: 75 */       String[] margs = new String[5];
/*  69:    */       
/*  70: 77 */       margs[0] = dom;
/*  71: 78 */       margs[1] = basedir;
/*  72: 79 */       margs[2] = ttd;
/*  73: 80 */       margs[3] = "0.14";
/*  74: 81 */       margs[4] = "1";
/*  75:    */       
/*  76:    */ 
/*  77: 84 */       Assign.main(margs);
/*  78:    */       
/*  79:    */ 
/*  80: 87 */       margs = new String[5];
/*  81: 88 */       margs[0] = dom;
/*  82: 89 */       margs[1] = basedir;
/*  83: 90 */       margs[2] = ttd;
/*  84: 91 */       margs[3] = "2";
/*  85: 92 */       margs[4] = "2";
/*  86:    */       
/*  87: 94 */       CreateVecs.main(margs);
/*  88:    */     }
/*  89: 99 */     if (emu == 3)
/*  90:    */     {
/*  91:102 */       String[] margs = new String[5];
/*  92:103 */       margs[0] = "lapt";
/*  93:104 */       margs[1] = basedir;
/*  94:105 */       margs[2] = ttd;
/*  95:106 */       margs[3] = "0.07";
/*  96:107 */       margs[4] = "2";
/*  97:108 */       Assign.main(margs);
/*  98:    */       
/*  99:    */ 
/* 100:111 */       margs = new String[4];
/* 101:112 */       margs[0] = (basedir + ttd + "/" + "teCln.PrdAspTrg.xml");
/* 102:113 */       margs[1] = (basedir + ttd + "/" + "teGld.xml");
/* 103:114 */       margs[2] = "2";
/* 104:115 */       margs[3] = "0";
/* 105:    */       
/* 106:    */ 
/* 107:118 */       ABSA15Eval.main(margs);
/* 108:    */       
/* 109:120 */       System.err.println();
/* 110:    */       
/* 111:    */ 
/* 112:123 */       margs[0] = (basedir + ttd + "/" + "teGld.xml");
/* 113:124 */       ABSA15Eval.main(margs);
/* 114:    */       
/* 115:    */ 
/* 116:127 */       margs = new String[4];
/* 117:128 */       margs[0] = (basedir + ttd + "/" + "teGldAspTrg.PrdPol.xml");
/* 118:129 */       margs[1] = (basedir + ttd + "/" + "teGld.xml");
/* 119:130 */       margs[2] = "2";
/* 120:131 */       margs[3] = "1";
/* 121:132 */       ABSA15Eval.main(margs);
/* 122:    */       
/* 123:134 */       System.err.println();
/* 124:    */     }
/* 125:    */   }
/* 126:    */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.BaseCmdEmu
 * JD-Core Version:    0.7.0.1
 */