package gr.ilsp.nlp.semeval.absa15.baselines;

import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
import gr.ilsp.nlp.utils.stopwords.StopWordsFromCSV;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

public class ExtractFeats
{
  private StopWordsFromCSV stopwords;
  private Settings settings;
  private int numOfFeats;
  
  public ExtractFeats(Settings settings, int numOfFeats)
  {
    this.settings = settings;
    this.numOfFeats = numOfFeats;
    
    this.stopwords = new StopWordsFromCSV();
    this.stopwords.load(getClass().getClassLoader().getResourceAsStream("gr/ilsp/nlp/semeval/absa15/common-english-words.txt"));
  }
  
  public void extractBagOfWords()
  {
    try
    {
      String train = this.settings.getTrain();
      String wordList = this.settings.getWords();
      
      System.err.println("Extract feats from:" + train + " and store them to " + wordList);
      
      ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
      ABSA15Reviews revs = unm.read(train);
      
      HashMap<String, Integer> freqs = new HashMap();
      for (int i = 0; i < revs.getReviews().size(); i++)
      {
        ABSA15Sentences sentences = ((ABSA15Review)revs.getReviews().get(i)).getSentences();
        for (int j = 0; j < sentences.getSentenceList().size(); j++)
        {
          ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
          if (sent.getOOS() == null)
          {
            String text = sent.getText();
            
            StringTokenizer st = new StringTokenizer(text, " ,.!'()", true);
            while (st.hasMoreTokens())
            {
              String token = st.nextToken();
              token = token.toLowerCase();
              if (freqs.get(token) == null)
              {
                freqs.put(token, Integer.valueOf(1));
              }
              else
              {
                int value = ((Integer)freqs.get(token)).intValue() + 1;
                freqs.put(token, Integer.valueOf(value));
              }
            }
          }
        }
      }
      ArrayList<TokenFreq> array = new ArrayList();
      Iterator<String> keys = freqs.keySet().iterator();
      while (keys.hasNext())
      {
        String tok = (String)keys.next();
        if ((!tok.equals(" ")) && (!tok.equals(",")) && 
          (!tok.equals(".")) && (!tok.equals("(")) && (!tok.equals(")")) && (!this.stopwords.isStopWord(tok)))
        {
          TokenFreq tf = new TokenFreq(tok, ((Integer)freqs.get(tok)).intValue());
          array.add(tf);
        }
      }
      Collections.sort(array, new TokenFreqComparator());
      

      array = new ArrayList(array.subList(0, this.numOfFeats));
      FileOutputStream wFOS = new FileOutputStream(wordList);
      for (int i = 0; i < array.size(); i++)
      {
        wFOS.write((((TokenFreq)array.get(i)).getToken() + "\n").getBytes());
        wFOS.flush();
      }
      wFOS.close();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
    
  public static void main(String[] args)
  {
    String d = args[0];
    String r = args[1];
    String TrainTest = args[2];
    int numOfFeats = Integer.parseInt(args[3]);
    
    Settings settings = null;
    int partIdx = 0;
    if (args.length == 5)
    {
      partIdx = Integer.parseInt(args[4]);
      settings = new Settings(r, TrainTest, "", "", "." + partIdx);
    }
    else
    {
      settings = new Settings(r, TrainTest, "", "", "");
    }
    ExtractFeats EF = new ExtractFeats(settings, numOfFeats);
    EF.extractBagOfWords();
  }
}
