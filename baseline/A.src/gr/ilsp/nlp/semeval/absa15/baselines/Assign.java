/*   1:    */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*   2:    */ 
/*   3:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
/*   4:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
/*   5:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
/*   6:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
/*   7:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
/*   8:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
/*   9:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
/*  10:    */ import gr.ilsp.nlp.semeval.absa15.xml.SemEval2015Marshaller;
/*  11:    */ import java.io.BufferedReader;
/*  12:    */ import java.io.File;
/*  13:    */ import java.io.FileReader;
/*  14:    */ import java.io.PrintStream;
/*  15:    */ import java.util.ArrayList;
/*  16:    */ import java.util.HashMap;
/*  17:    */ import java.util.LinkedHashMap;
/*  18:    */ import javax.xml.bind.Marshaller;
/*  19:    */ 
/*  20:    */ public class Assign
/*  21:    */ {
/*  22:    */   public ABSA15Reviews decide(Settings settings, float thres, int stage, int pred)
/*  23:    */     throws Exception
/*  24:    */   {
/*  25: 25 */     String testXML = "";
/*  26: 26 */     String out = "";
/*  27: 27 */     String predicted = "";
/*  28: 29 */     if (stage == 1)
/*  29:    */     {
/*  30: 31 */       testXML = settings.getTeCln();
/*  31: 32 */       out = settings.getOutAsp();
/*  32: 33 */       predicted = settings.getTeCln_PrdAspTrg();
/*  33:    */     }
/*  34: 36 */     if (stage == 2) {
/*  35: 38 */       if (pred == 0)
/*  36:    */       {
/*  37: 40 */         testXML = settings.getTeGldAspTrg();
/*  38: 41 */         out = settings.getOutPol();
/*  39: 42 */         predicted = settings.getTeGldAspTrg_PrdPol();
/*  40:    */       }
/*  41: 44 */       else if (pred == 1)
/*  42:    */       {
/*  43: 46 */         testXML = settings.getTeCln_PrdAspTrg();
/*  44: 47 */         out = settings.getOutPol();
/*  45: 48 */         predicted = settings.getTeCln_PrdAspTrgs_PrdPol();
/*  46:    */       }
/*  47:    */     }
/*  48: 52 */     System.err.println("Assigning to :" + testXML + " using the predictions that are stored in" + out);
/*  49: 53 */     System.err.println("Output :" + predicted);
/*  50:    */     
/*  51: 55 */     ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
/*  52: 56 */     ABSA15Reviews revs = unm.read(testXML);
/*  53:    */     
/*  54: 58 */     AspectsMapping AM = settings.getAM();
/*  55:    */     
/*  56: 60 */     BufferedReader reader = new BufferedReader(new FileReader(out));
/*  57: 61 */     String line = reader.readLine();
/*  58:    */     
/*  59: 63 */     String[] labels = line.split(" ");
/*  60:    */     
/*  61: 65 */     int predictionsRead = 0;
/*  62: 66 */     for (int i = 0; i < revs.getReviews().size(); i++)
/*  63:    */     {
/*  64: 68 */       ABSA15Sentences sentences = ((ABSA15Review)revs.getReviews().get(i)).getSentences();
/*  65: 70 */       for (int j = 0; j < sentences.getSentenceList().size(); j++)
/*  66:    */       {
/*  67: 72 */         ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/*  68: 75 */         if (sent.getOOS() == null)
/*  69:    */         {
/*  70: 78 */           if (stage == 1)
/*  71:    */           {
/*  72: 82 */             line = reader.readLine();
/*  73: 83 */             predictionsRead++;
/*  74: 84 */             String[] t = line.split(" ");
/*  75:    */             
/*  76:    */ 
/*  77: 87 */             float sum = 0.0F;
/*  78: 88 */             float max = 0.0F;
/*  79: 89 */             int argmax = -1;
/*  80: 91 */             for (int k = 1; k <= t.length - 1; k++)
/*  81:    */             {
/*  82: 93 */               float score = Float.parseFloat(t[k]);
/*  83: 94 */               if (score > max)
/*  84:    */               {
/*  85: 96 */                 max = score;
/*  86: 97 */                 argmax = k;
/*  87:    */               }
/*  88:100 */               sum += score;
/*  89:    */             }
/*  90:103 */             for (int k = 1; k <= t.length - 1; k++)
/*  91:    */             {
/*  92:105 */               float score = Float.parseFloat(t[k]);
/*  93:107 */               if (score >= thres)
/*  94:    */               {
/*  95:109 */                 ABSA15Opinion op = new ABSA15Opinion();
/*  96:110 */                 op.setCategory((String)AM.getNumtoLabel().get(Integer.valueOf(Integer.parseInt(labels[k]))));
/*  97:111 */                 op.setTarget("NULL");
/*  98:112 */                 op.setPolarity("positive");
/*  99:113 */                 sent.getOp().getOpiList().add(op);
/* 100:    */               }
/* 101:    */             }
/* 102:    */           }
/* 103:120 */           if (stage == 2) {
/* 104:122 */             for (int k = 0; k < sent.getOp().getOpiList().size(); k++)
/* 105:    */             {
/* 106:124 */               line = reader.readLine();
/* 107:125 */               predictionsRead++;
/* 108:126 */               String[] t = line.split(" ");
/* 109:    */               
/* 110:128 */               ABSA15Opinion o = (ABSA15Opinion)sent.getOp().getOpiList().get(k);
/* 111:129 */               int n = Integer.parseInt(t[0]);
/* 112:130 */               String pol = (String)PolarityLabelsMapping.rmap.get(Integer.valueOf(n));
/* 113:131 */               o.setPolarity(pol);
/* 114:    */             }
/* 115:    */           }
/* 116:    */         }
/* 117:    */       }
/* 118:    */     }
/* 119:141 */     SemEval2015Marshaller marhshaller = new SemEval2015Marshaller();
/* 120:142 */     Marshaller jaxbMarshaller = marhshaller.getJaxbMarshaller();
/* 121:143 */     File output = new File(predicted);
/* 122:144 */     jaxbMarshaller.marshal(revs, output);
/* 123:    */     
/* 124:146 */     System.err.println("predictionsRead = " + predictionsRead);
/* 125:147 */     return revs;
/* 126:    */   }
/* 127:    */   
/* 128:    */   public static void main(String[] args)
/* 129:    */   {
/* 130:    */     try
/* 131:    */     {
/* 132:155 */       String domain = args[0];
/* 133:156 */       String rootDir = args[1];
/* 134:157 */       String TrainTest = args[2];
/* 135:158 */       float stage1Thr = Float.parseFloat(args[3]);
/* 136:159 */       int stage = Integer.parseInt(args[4]);
/* 137:160 */       int pred = Integer.parseInt(args[5]);
/* 138:    */       
/* 139:162 */       Settings settings = null;
/* 140:163 */       int partIdx = 0;
/* 141:165 */       if (args.length == 7)
/* 142:    */       {
/* 143:167 */         partIdx = Integer.parseInt(args[6]);
/* 144:168 */         settings = new Settings(rootDir, TrainTest, domain, "", "." + partIdx);
/* 145:    */       }
/* 146:    */       else
/* 147:    */       {
/* 148:172 */         settings = new Settings(rootDir, TrainTest, domain, "", "");
/* 149:    */       }
/* 150:175 */       Assign decide = new Assign();
/* 151:176 */       decide.decide(settings, stage1Thr, stage, pred);
/* 152:    */     }
/* 153:    */     catch (Exception e)
/* 154:    */     {
/* 155:181 */       e.printStackTrace();
/* 156:    */     }
/* 157:    */   }
/* 158:    */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.Assign
 * JD-Core Version:    0.7.0.1
 */