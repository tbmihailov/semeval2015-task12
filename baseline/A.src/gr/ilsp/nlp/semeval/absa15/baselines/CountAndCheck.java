/*   1:    */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*   2:    */ 
/*   3:    */ import gr.ilsp.nlp.semeval.absa.eval.ConsoleXmlValidator;
/*   4:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
/*   5:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
/*   6:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
/*   7:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
/*   8:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
/*   9:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
/*  10:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
/*  11:    */ import java.io.PrintStream;
/*  12:    */ import java.util.ArrayList;
/*  13:    */ import java.util.HashMap;
/*  14:    */ import java.util.HashSet;
/*  15:    */ import java.util.LinkedHashMap;
/*  16:    */ 
/*  17:    */ public class CountAndCheck
/*  18:    */ {
/*  19:    */   private static void update(HashMap<String, Integer> f, String key)
/*  20:    */   {
/*  21: 19 */     Integer c = (Integer)f.get(key);
/*  22: 21 */     if (c != null) {
/*  23: 23 */       f.put(key, Integer.valueOf(c.intValue() + 1));
/*  24:    */     } else {
/*  25: 27 */       f.put(key, Integer.valueOf(1));
/*  26:    */     }
/*  27:    */   }
/*  28:    */   
/*  29:    */   private static void print(HashMap<String, Integer> f, Aspects Asp)
/*  30:    */   {
/*  31: 34 */     int sum = 0;
/*  32:    */     
/*  33: 36 */     System.err.print("\t");
/*  34: 38 */     for (int j = 0; j < Asp.getB().length; j++) {
/*  35: 40 */       System.err.print(Asp.getB()[j] + "\t");
/*  36:    */     }
/*  37: 43 */     System.err.println();
/*  38: 46 */     for (int i = 0; i < Asp.getA().length; i++)
/*  39:    */     {
/*  40: 48 */       System.err.print(Asp.getA()[i] + "\t");
/*  41: 49 */       for (int j = 0; j < Asp.getB().length; j++)
/*  42:    */       {
/*  43: 51 */         String cat = Asp.getA()[i] + "#" + Asp.getB()[j];
/*  44:    */         
/*  45: 53 */         Integer c = (Integer)f.get(cat);
/*  46: 55 */         if (c != null)
/*  47:    */         {
/*  48: 57 */           System.err.print(c.intValue() + "\t");
/*  49: 58 */           sum += c.intValue();
/*  50:    */         }
/*  51:    */         else
/*  52:    */         {
/*  53: 62 */           System.err.print("0\t");
/*  54:    */         }
/*  55:    */       }
/*  56: 65 */       System.err.println();
/*  57:    */     }
/*  58: 68 */     System.err.println("\n Sum of all valid category occurences = " + sum);
/*  59:    */   }
/*  60:    */   
/*  61:    */   public static void check(String xml, String xsd, String dom)
/*  62:    */   {
/*  63:    */     try
/*  64:    */     {
/*  65: 75 */       int missingTrgs = 0;
/*  66: 76 */       int missingCats = 0;
/*  67:    */       
/*  68: 78 */       int blankCats = 0;
/*  69: 79 */       int blankTrgs = 0;
/*  70:    */       
/*  71: 81 */       Settings sts = new Settings("", "", dom, "", "");
/*  72: 82 */       String[] mmargs = new String[2];
/*  73: 83 */       mmargs[0] = xml;
/*  74: 84 */       mmargs[1] = xsd;
/*  75: 85 */       ConsoleXmlValidator.main(mmargs);
/*  76:    */       
/*  77: 87 */       System.err.println("\n\n");
/*  78: 88 */       AspectsMapping AM = sts.getAM();
/*  79:    */       
/*  80: 90 */       HashMap<String, Integer> catFreqs = new HashMap();
/*  81: 91 */       HashMap<String, Integer> polFreqs = new HashMap();
/*  82:    */       
/*  83: 93 */       ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
/*  84: 94 */       ABSA15Reviews revs = unm.read(xml);
/*  85:    */       
/*  86: 96 */       int count_opi = 0;
/*  87:    */       
/*  88: 98 */       HashSet<String> sent_set = new HashSet();
/*  89: 99 */       HashSet<String> revs_set = new HashSet();
/*  90:101 */       for (int i = 0; i < revs.getReviews().size(); i++)
/*  91:    */       {
/*  92:103 */         ABSA15Review r = (ABSA15Review)revs.getReviews().get(i);
/*  93:104 */         ABSA15Sentences sentences = r.getSentences();
/*  94:106 */         if (!revs_set.contains(r.getRid())) {
/*  95:108 */           revs_set.add(r.getRid());
/*  96:    */         } else {
/*  97:112 */           System.err.println("!!!!duplicate " + r.getRid());
/*  98:    */         }
/*  99:115 */         for (int j = 0; j < sentences.getSentenceList().size(); j++)
/* 100:    */         {
/* 101:117 */           ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/* 102:118 */           String text = sent.getText();
/* 103:120 */           if (!sent_set.contains(sent.getId())) {
/* 104:122 */             sent_set.add(sent.getId());
/* 105:    */           } else {
/* 106:126 */             System.err.println("!!!!duplicate " + sent.getId());
/* 107:    */           }
/* 108:129 */           if (sent.getId().indexOf(" ") != -1) {
/* 109:131 */             System.err.println("!!!!id issue" + sent.getId());
/* 110:    */           }
/* 111:134 */           if (sent.getOOS() == null) {
/* 112:136 */             for (int k = 0; k < sent.getOp().getOpiList().size(); k++)
/* 113:    */             {
/* 114:138 */               count_opi++;
/* 115:    */               
/* 116:140 */               ABSA15Opinion o = (ABSA15Opinion)sent.getOp().getOpiList().get(k);
/* 117:    */               
/* 118:    */ 
/* 119:    */ 
/* 120:144 */               String cat = o.getCategory();
/* 121:146 */               if (cat != null)
/* 122:    */               {
/* 123:148 */                 if (cat.equals("")) {
/* 124:150 */                   blankCats++;
/* 125:    */                 }
/* 126:153 */                 Integer intLabel = (Integer)AM.getLabeltoNum().get(cat);
/* 127:155 */                 if (intLabel != null)
/* 128:    */                 {
/* 129:157 */                   int label = ((Integer)AM.getLabeltoNum().get(cat)).intValue();
/* 130:158 */                   update(catFreqs, cat);
/* 131:    */                 }
/* 132:    */                 else
/* 133:    */                 {
/* 134:162 */                   System.err.println("!!!!CAT ISSUE:" + r.getRid() + " sent:" + j + " " + cat);
/* 135:    */                 }
/* 136:    */               }
/* 137:    */               else
/* 138:    */               {
/* 139:167 */                 missingCats++;
/* 140:    */               }
/* 141:173 */               if (o.getTarget() != null)
/* 142:    */               {
/* 143:175 */                 if (o.getTarget().equals("")) {
/* 144:177 */                   blankTrgs++;
/* 145:    */                 }
/* 146:180 */                 if (!o.getTarget().equals("NULL"))
/* 147:    */                 {
/* 148:182 */                   String offsets_trg = text.substring(o.getFrom(), o.getTo());
/* 149:184 */                   if (!offsets_trg.equalsIgnoreCase(o.getTarget())) {
/* 150:186 */                     System.err.println("!!!!OFF ISSUE:" + r.getRid() + " sent:" + j + " trg:" + offsets_trg);
/* 151:    */                   }
/* 152:    */                 }
/* 153:192 */                 else if ((o.getFrom() != 0) || (o.getTo() != 0))
/* 154:    */                 {
/* 155:194 */                   System.err.println("!!!!OFF ISSUE:" + r.getRid() + " sent:" + j + " not 0 0");
/* 156:    */                 }
/* 157:    */               }
/* 158:    */               else
/* 159:    */               {
/* 160:200 */                 missingTrgs++;
/* 161:    */               }
/* 162:206 */               Integer pol = (Integer)PolarityLabelsMapping.map.get(o.getPolarity());
/* 163:208 */               if (pol != null) {
/* 164:210 */                 update(polFreqs, o.getPolarity());
/* 165:    */               } else {
/* 166:214 */                 System.err.println("!!!!POL ISSUE:" + r.getRid() + " sent:" + j + " " + pol);
/* 167:    */               }
/* 168:    */             }
/* 169:221 */           } else if (!sent.getOOS().equals("TRUE")) {
/* 170:223 */             System.err.println("!!!!!OOS ISSUE:" + r.getRid() + " sent:" + j + " ");
/* 171:    */           }
/* 172:    */         }
/* 173:    */       }
/* 174:229 */       System.err.println("\n\n\n");
/* 175:230 */       print(catFreqs, sts.getAsp());
/* 176:    */       
/* 177:232 */       System.err.println("\n\n total opinions found " + count_opi);
/* 178:    */       
/* 179:234 */       System.err.println(" #missing targets " + missingTrgs);
/* 180:235 */       System.err.println(" #missing categories " + missingCats);
/* 181:    */       
/* 182:237 */       System.err.println(" #targets with blank value " + blankTrgs);
/* 183:238 */       System.err.println(" #categories with blank value " + blankCats);
/* 184:    */       
/* 185:240 */       System.err.println("**********************************************************\n\n\n\n");
/* 186:    */     }
/* 187:    */     catch (Exception e)
/* 188:    */     {
/* 189:244 */       e.printStackTrace();
/* 190:    */     }
/* 191:    */   }
/* 192:    */   
/* 193:    */   public static void main(String[] args)
/* 194:    */   {
/* 195:252 */     String xmlFile = "";
/* 196:253 */     String xsdFile = "";
/* 197:254 */     String dom = "";
/* 198:    */     
/* 199:256 */     xmlFile = args[0];
/* 200:257 */     xsdFile = args[1];
/* 201:258 */     dom = args[2];
/* 202:    */     
/* 203:260 */     CountAndCheck CC = new CountAndCheck();
/* 204:261 */     check(xmlFile, xsdFile, dom);
/* 205:    */   }
/* 206:    */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.CountAndCheck
 * JD-Core Version:    0.7.0.1
 */