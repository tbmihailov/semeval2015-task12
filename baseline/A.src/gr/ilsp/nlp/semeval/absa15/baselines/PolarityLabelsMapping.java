/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ import java.util.LinkedHashMap;
/*  4:   */ 
/*  5:   */ public class PolarityLabelsMapping
/*  6:   */ {
/*  7: 7 */   public static LinkedHashMap<String, Integer> map = new LinkedHashMap();
/*  8: 8 */   public static LinkedHashMap<Integer, String> rmap = new LinkedHashMap();
/*  9:   */   
/* 10:   */   static
/* 11:   */   {
/* 12:12 */     map.put("positive", Integer.valueOf(0));
/* 13:13 */     map.put("negative", Integer.valueOf(1));
/* 14:14 */     map.put("neutral", Integer.valueOf(2));
/* 15:15 */     map.put("conflict", Integer.valueOf(3));
/* 16:   */     
/* 17:17 */     rmap.put(Integer.valueOf(0), "positive");
/* 18:18 */     rmap.put(Integer.valueOf(1), "negative");
/* 19:19 */     rmap.put(Integer.valueOf(2), "neutral");
/* 20:20 */     rmap.put(Integer.valueOf(3), "conflict");
/* 21:   */   }
/* 22:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.PolarityLabelsMapping
 * JD-Core Version:    0.7.0.1
 */