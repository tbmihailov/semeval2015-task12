/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ public class AspectsMappingLapt
/*  4:   */   extends Aspects
/*  5:   */ {
/*  6: 6 */   public static final String[] A = {
/*  7: 7 */     "LAPTOP", "BATTERY", "CPU", "GRAPHICS", "HARD_DISC", "OS", "SUPPORT", "COMPANY", 
/*  8: 8 */     "DISPLAY", "MOUSE", "SOFTWARE", "KEYBOARD", "OPTICAL_DRIVES", "WARRANTY", "MULTIMEDIA_DEVICES", "PORTS", 
/*  9: 9 */     "POWER_SUPPLY", "HARDWARE", "SHIPPING", "MEMORY", "MOTHERBOARD", "FANS_COOLING" };
/* 10:13 */   public static final String[] B = {
/* 11:14 */     "GENERAL", "OPERATION_PERFORMANCE", "DESIGN_FEATURES", "USABILITY", 
/* 12:15 */     "PORTABILITY", "PRICE", "QUALITY", "MISCELLANEOUS", "CONNECTIVITY" };
/* 13:   */   
/* 14:   */   public String[] getA()
/* 15:   */   {
/* 16:19 */     return A;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public String[] getB()
/* 20:   */   {
/* 21:23 */     return B;
/* 22:   */   }
/* 23:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.AspectsMappingLapt
 * JD-Core Version:    0.7.0.1
 */