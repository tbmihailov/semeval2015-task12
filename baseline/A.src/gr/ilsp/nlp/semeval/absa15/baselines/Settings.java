/*   1:    */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*   2:    */ 
/*   3:    */ public class Settings
/*   4:    */ {
/*   5:    */   public static final String LAPT = "lapt";
/*   6:    */   public static final String REST = "rest";
/*   7:    */   private AspectsMapping AM;
/*   8:    */   private Aspects asp;
/*   9:    */   public static final String TR = "tr.xml";
/*  10:    */   public static final String TEGldAll = "teGld.xml";
/*  11:    */   public static final String TECln = "teCln.xml";
/*  12:    */   public static final String TEGoldAspTrg = "teGldAspTrg.xml";
/*  13:    */   public static final String TECln_PrdAspTrg = "teCln.PrdAspTrg.xml";
/*  14:    */   public static final String TECln_PrdAspTrg_PrdPol = "teCln.PrdAspTrg.PrdPol.xml";
/*  15:    */   public static final String TEGoldAspTrg_PrdPol = "teGldAspTrg.PrdPol.xml";
/*  16:    */   public static final String TRLibLin = "tr.svm";
/*  17:    */   public static final String TELibLin = "te.svm";
/*  18:    */   public static final String O = "Out";
/*  19:    */   public static final String W = "Feats";
/*  20: 31 */   private String partSuff = "";
/*  21:    */   private String train;
/*  22:    */   private String teCln;
/*  23:    */   private String teGldAll;
/*  24:    */   private String teGldAspTrg;
/*  25:    */   private String teCln_PrdAspTrgs;
/*  26:    */   private String teCln_PrdAspTrgs_PrdPol;
/*  27:    */   private String teGldAspTrg_PrdPol;
/*  28:    */   private String trFeatFileA;
/*  29:    */   private String teFeatFileA;
/*  30:    */   private String trFeatFileP;
/*  31:    */   private String teFeatFileP4Gold;
/*  32:    */   private String teFeatFileP4Pred;
/*  33:    */   private String outAsp;
/*  34:    */   private String outPol;
/*  35:    */   private String words;
/*  36:    */   private String labels;
/*  37:    */   private String inputFullPath;
/*  38:    */   private String input;
/*  39:    */   private String domain;
/*  40:    */   
/*  41:    */   public Settings(String r, String TrainTest, String dom, String input, String partSuff)
/*  42:    */   {
/*  43: 69 */     this.domain = dom;
/*  44: 70 */     this.partSuff = partSuff;
/*  45:    */     
/*  46: 72 */     this.AM = new AspectsMapping();
/*  47: 74 */     if (dom.equals("lapt"))
/*  48:    */     {
/*  49: 76 */       this.asp = new AspectsMappingLapt();
/*  50: 77 */       this.AM.load(this.asp);
/*  51:    */     }
/*  52: 80 */     if (dom.equals("rest"))
/*  53:    */     {
/*  54: 82 */       this.asp = new AspectsMappingRest();
/*  55: 83 */       this.AM.load(this.asp);
/*  56:    */     }
/*  57: 86 */     String tt = r + TrainTest + System.getProperty("file.separator");
/*  58:    */     
/*  59:    */ 
/*  60: 89 */     this.input = input;
/*  61: 90 */     this.inputFullPath = (r + input);
/*  62:    */     
/*  63: 92 */     this.train = (tt + "tr.xml" + partSuff);
/*  64: 93 */     this.teCln = (tt + "teCln.xml" + partSuff);
/*  65: 94 */     this.teGldAll = (tt + "teGld.xml" + partSuff);
/*  66: 95 */     this.teGldAspTrg = (tt + "teGldAspTrg.xml" + partSuff);
/*  67:    */     
/*  68: 97 */     this.teGldAspTrg_PrdPol = (tt + "teGldAspTrg.PrdPol.xml" + partSuff);
/*  69: 98 */     this.teCln_PrdAspTrgs = (tt + "teCln.PrdAspTrg.xml" + partSuff);
/*  70: 99 */     this.teCln_PrdAspTrgs_PrdPol = (tt + "teCln.PrdAspTrg.PrdPol.xml" + partSuff);
/*  71:    */     
/*  72:    */ 
/*  73:102 */     this.trFeatFileA = (tt + "tr.svm" + ".asp" + partSuff);
/*  74:103 */     this.teFeatFileA = (tt + "te.svm" + ".asp" + partSuff);
/*  75:104 */     this.trFeatFileP = (tt + "tr.svm" + ".pol" + partSuff);
/*  76:    */     
/*  77:106 */     this.teFeatFileP4Gold = (tt + "te.svm" + ".pol4g" + partSuff);
/*  78:107 */     this.teFeatFileP4Pred = (tt + "te.svm" + ".pol4p" + partSuff);
/*  79:    */     
/*  80:109 */     this.outAsp = (tt + "Out" + ".asp" + partSuff);
/*  81:110 */     this.outPol = (tt + "Out" + ".pol" + partSuff);
/*  82:    */     
/*  83:112 */     this.words = (tt + "Feats" + partSuff);
/*  84:    */   }
/*  85:    */   
/*  86:    */   public String getPartSuff()
/*  87:    */   {
/*  88:117 */     return this.partSuff;
/*  89:    */   }
/*  90:    */   
/*  91:    */   public void setPartSuff(String partSuff)
/*  92:    */   {
/*  93:122 */     this.partSuff = partSuff;
/*  94:    */   }
/*  95:    */   
/*  96:    */   public Aspects getAsp()
/*  97:    */   {
/*  98:127 */     return this.asp;
/*  99:    */   }
/* 100:    */   
/* 101:    */   public void setAsp(Aspects asp)
/* 102:    */   {
/* 103:132 */     this.asp = asp;
/* 104:    */   }
/* 105:    */   
/* 106:    */   public AspectsMapping getAM()
/* 107:    */   {
/* 108:137 */     return this.AM;
/* 109:    */   }
/* 110:    */   
/* 111:    */   public void setAM(AspectsMapping aM)
/* 112:    */   {
/* 113:142 */     this.AM = aM;
/* 114:    */   }
/* 115:    */   
/* 116:    */   public String getDomain()
/* 117:    */   {
/* 118:147 */     return this.domain;
/* 119:    */   }
/* 120:    */   
/* 121:    */   public void setDomain(String domain)
/* 122:    */   {
/* 123:151 */     this.domain = domain;
/* 124:    */   }
/* 125:    */   
/* 126:    */   public String getTrain()
/* 127:    */   {
/* 128:155 */     return this.train;
/* 129:    */   }
/* 130:    */   
/* 131:    */   public void setTrain(String train)
/* 132:    */   {
/* 133:159 */     this.train = train;
/* 134:    */   }
/* 135:    */   
/* 136:    */   public String getTeCln()
/* 137:    */   {
/* 138:163 */     return this.teCln;
/* 139:    */   }
/* 140:    */   
/* 141:    */   public void setTeCln(String teCln)
/* 142:    */   {
/* 143:167 */     this.teCln = teCln;
/* 144:    */   }
/* 145:    */   
/* 146:    */   public String getTeGldAll()
/* 147:    */   {
/* 148:171 */     return this.teGldAll;
/* 149:    */   }
/* 150:    */   
/* 151:    */   public void setTeGldAll(String teGldAll)
/* 152:    */   {
/* 153:175 */     this.teGldAll = teGldAll;
/* 154:    */   }
/* 155:    */   
/* 156:    */   public String getTeGldAspTrg()
/* 157:    */   {
/* 158:179 */     return this.teGldAspTrg;
/* 159:    */   }
/* 160:    */   
/* 161:    */   public void setTeGldAspTrg(String teGldAsp)
/* 162:    */   {
/* 163:183 */     this.teGldAspTrg = teGldAsp;
/* 164:    */   }
/* 165:    */   
/* 166:    */   public String getTeCln_PrdAspTrg()
/* 167:    */   {
/* 168:187 */     return this.teCln_PrdAspTrgs;
/* 169:    */   }
/* 170:    */   
/* 171:    */   public void setTeCln_PrdAspTrg(String teCln_PrdAsp)
/* 172:    */   {
/* 173:191 */     this.teCln_PrdAspTrgs = teCln_PrdAsp;
/* 174:    */   }
/* 175:    */   
/* 176:    */   public String getTrFeatFileA()
/* 177:    */   {
/* 178:195 */     return this.trFeatFileA;
/* 179:    */   }
/* 180:    */   
/* 181:    */   public void setTrFeatFileA(String trFeatFileA)
/* 182:    */   {
/* 183:199 */     this.trFeatFileA = trFeatFileA;
/* 184:    */   }
/* 185:    */   
/* 186:    */   public String getTeFeatFileA()
/* 187:    */   {
/* 188:203 */     return this.teFeatFileA;
/* 189:    */   }
/* 190:    */   
/* 191:    */   public void setTeFeatFileA(String teFeatFileA)
/* 192:    */   {
/* 193:207 */     this.teFeatFileA = teFeatFileA;
/* 194:    */   }
/* 195:    */   
/* 196:    */   public String getTrFeatFileP()
/* 197:    */   {
/* 198:211 */     return this.trFeatFileP;
/* 199:    */   }
/* 200:    */   
/* 201:    */   public void setTrFeatFileP(String trFeatFileP)
/* 202:    */   {
/* 203:215 */     this.trFeatFileP = trFeatFileP;
/* 204:    */   }
/* 205:    */   
/* 206:    */   public String getTeFeatFileP4Gold()
/* 207:    */   {
/* 208:219 */     return this.teFeatFileP4Gold;
/* 209:    */   }
/* 210:    */   
/* 211:    */   public void setTeFeatFileP4Gold(String teFeatFileP4Gold)
/* 212:    */   {
/* 213:224 */     this.teFeatFileP4Gold = teFeatFileP4Gold;
/* 214:    */   }
/* 215:    */   
/* 216:    */   public String getTeFeatFileP4Pred()
/* 217:    */   {
/* 218:229 */     return this.teFeatFileP4Pred;
/* 219:    */   }
/* 220:    */   
/* 221:    */   public void setTeFeatFileP4Pred(String teFeatFileP4Pred)
/* 222:    */   {
/* 223:234 */     this.teFeatFileP4Pred = teFeatFileP4Pred;
/* 224:    */   }
/* 225:    */   
/* 226:    */   public String getOutAsp()
/* 227:    */   {
/* 228:238 */     return this.outAsp;
/* 229:    */   }
/* 230:    */   
/* 231:    */   public void setOutAsp(String outAsp)
/* 232:    */   {
/* 233:242 */     this.outAsp = outAsp;
/* 234:    */   }
/* 235:    */   
/* 236:    */   public String getOutPol()
/* 237:    */   {
/* 238:246 */     return this.outPol;
/* 239:    */   }
/* 240:    */   
/* 241:    */   public void setOutPol(String outPol)
/* 242:    */   {
/* 243:250 */     this.outPol = outPol;
/* 244:    */   }
/* 245:    */   
/* 246:    */   public String getWords()
/* 247:    */   {
/* 248:254 */     return this.words;
/* 249:    */   }
/* 250:    */   
/* 251:    */   public void setWords(String words)
/* 252:    */   {
/* 253:258 */     this.words = words;
/* 254:    */   }
/* 255:    */   
/* 256:    */   public String getLabels()
/* 257:    */   {
/* 258:262 */     return this.labels;
/* 259:    */   }
/* 260:    */   
/* 261:    */   public void setLabels(String labels)
/* 262:    */   {
/* 263:266 */     this.labels = labels;
/* 264:    */   }
/* 265:    */   
/* 266:    */   public String getInputFullPath()
/* 267:    */   {
/* 268:270 */     return this.inputFullPath;
/* 269:    */   }
/* 270:    */   
/* 271:    */   public void setInputFullPath(String inputFullPath)
/* 272:    */   {
/* 273:274 */     this.inputFullPath = inputFullPath;
/* 274:    */   }
/* 275:    */   
/* 276:    */   public String getInput()
/* 277:    */   {
/* 278:278 */     return this.input;
/* 279:    */   }
/* 280:    */   
/* 281:    */   public void setInput(String input)
/* 282:    */   {
/* 283:282 */     this.input = input;
/* 284:    */   }
/* 285:    */   
/* 286:    */   public String getTeGldAspTrg_PrdPol()
/* 287:    */   {
/* 288:286 */     return this.teGldAspTrg_PrdPol;
/* 289:    */   }
/* 290:    */   
/* 291:    */   public void setTeGldAspTrg_PrdPol(String teGldAsp_PrdPol)
/* 292:    */   {
/* 293:290 */     this.teGldAspTrg_PrdPol = teGldAsp_PrdPol;
/* 294:    */   }
/* 295:    */   
/* 296:    */   public String getTeCln_PrdAspTrgs_PrdPol()
/* 297:    */   {
/* 298:295 */     return this.teCln_PrdAspTrgs_PrdPol;
/* 299:    */   }
/* 300:    */   
/* 301:    */   public void setTeCln_PrdAspTrgs_PrdPol(String teCln_PrdAspTrgs_PrdPol)
/* 302:    */   {
/* 303:300 */     this.teCln_PrdAspTrgs_PrdPol = teCln_PrdAspTrgs_PrdPol;
/* 304:    */   }
/* 305:    */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.Settings
 * JD-Core Version:    0.7.0.1
 */