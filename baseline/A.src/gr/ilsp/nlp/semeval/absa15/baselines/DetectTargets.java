package gr.ilsp.nlp.semeval.absa15.baselines;

import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
import gr.ilsp.nlp.semeval.absa15.xml.SemEval2015Marshaller;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.xml.bind.Marshaller;

public class DetectTargets {
	private HashMap<String, HashSet<String>> targetsLists = new HashMap();
	private Settings settings;

	public DetectTargets(Settings settings) {
		this.settings = settings;
	}

	public void createTargetListFromTrain() {
		try {
			this.targetsLists = new HashMap();

			String train = this.settings.getTrain();

			ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
			ABSA15Reviews revs = unm.read(train);

			System.err.println("Initialize lists");
			for (int i = 0; i < this.settings.getAsp().getA().length; i++) {
				for (int j = 0; j < this.settings.getAsp().getB().length; j++) {
					String key = this.settings.getAsp().getA()[i] + "#"
							+ this.settings.getAsp().getB()[j];
					this.targetsLists.put(key, new HashSet());
				}
			}
			System.err.println("Create lists from:" + train);
			for (int i = 0; i < revs.getReviews().size(); i++) {
				ABSA15Sentences sentences = ((ABSA15Review) revs.getReviews()
						.get(i)).getSentences();
				for (int j = 0; j < sentences.getSentenceList().size(); j++) {
					ABSA15Sentence sent = (ABSA15Sentence) sentences
							.getSentenceList().get(j);
					if (sent.getOOS() == null) {
						for (int k = 0; k < sent.getOp().getOpiList().size(); k++) {
							ABSA15Opinion o = (ABSA15Opinion) sent.getOp()
									.getOpiList().get(k);
							String cat = o.getCategory();
							if ((o.getTarget() != null)
									&& (!o.getTarget().equals("NULL"))) {
								HashSet<String> set = (HashSet) this.targetsLists
										.get(cat);
								set.add(o.getTarget().toLowerCase());

								this.targetsLists.put(cat, set);
							}
						}
					}
				}
			}
			System.err.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void assign() {
		try {
			System.err.println("Assigning targets");
			String punctutations = ".,:;!";
			String test = this.settings.getTeCln_PrdAspTrg();

			ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
			ABSA15Reviews revs = unm.read(test);
			for (int i = 0; i < revs.getReviews().size(); i++) {
				ABSA15Sentences sentences = ((ABSA15Review) revs.getReviews()
						.get(i)).getSentences();
				for (int j = 0; j < sentences.getSentenceList().size(); j++) {
					ABSA15Sentence sent = (ABSA15Sentence) sentences
							.getSentenceList().get(j);
					String text = sent.getText();
					if (sent.getOOS() == null) {
						for (int k = 0; k < sent.getOp().getOpiList().size(); k++) {
							ABSA15Opinion o = (ABSA15Opinion) sent.getOp()
									.getOpiList().get(k);

							String cat = o.getCategory();

							HashSet<String> list = (HashSet) this.targetsLists
									.get(cat);
							Iterator<String> it = list.iterator();

							int[] sttIdxs = new int[list.size()];
							int[] endIdxs = new int[list.size()];
							String[] targets = new String[list.size()];

							int mentionIdx = 0;
							while (it.hasNext()) {
								String mention = (String) it.next();

								targets[mentionIdx] = mention;
								sttIdxs[mentionIdx] = 2147483647;
								endIdxs[mentionIdx] = 2147483647;

								text = text.toLowerCase();

								boolean found = false;

								int startSearch = 0;
								int startIdx = text.indexOf(mention,
										startSearch);
								int endIdx = startIdx + mention.length();
								while ((!found) && (startIdx != -1)) {
									boolean limitsOk = true;
									if (startIdx != -1) {
										if (startIdx != 0) {
											Character ch = Character
													.valueOf(text
															.charAt(startIdx - 1));
											if ((!Character.isWhitespace(ch
													.charValue()))
													&& (!punctutations
															.contains(String
																	.valueOf(ch)))) {
												limitsOk = false;
											}
										}
										if (endIdx != text.length()) {
											Character ch = Character
													.valueOf(text
															.charAt(endIdx));
											if ((!Character.isWhitespace(ch
													.charValue()))
													&& (!punctutations
															.contains(String
																	.valueOf(ch)))) {
												limitsOk = false;
											}
										}
										if (limitsOk) {
											sttIdxs[mentionIdx] = startIdx;
											endIdxs[mentionIdx] = endIdx;
											found = true;
										}
									}
									if (!found) {
										startSearch = endIdx;
										startIdx = text.indexOf(mention,
												startSearch);
										endIdx = startIdx + mention.length();
									}
								}
								mentionIdx++;
							}
							int argmin = -1;
							int min = 2147483647;
							for (mentionIdx = 0; mentionIdx < sttIdxs.length; mentionIdx++) {
								if (sttIdxs[mentionIdx] < min) {
									min = sttIdxs[mentionIdx];
									argmin = mentionIdx;
								}
							}
							if (argmin != -1) {
								o.setFrom(sttIdxs[argmin]);
								o.setTo(endIdxs[argmin]);
								o.setTarget(text.substring(sttIdxs[argmin],
										endIdxs[argmin]));
							} else {
								o.setTarget("NULL");
							}
						}
					}
				}
			}
			SemEval2015Marshaller marhshaller = new SemEval2015Marshaller();
			Marshaller jaxbMarshaller = marhshaller.getJaxbMarshaller();
			File output = new File(test);
			jaxbMarshaller.marshal(revs, output);
			System.err.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			String d = args[0];
			String r = args[1];
			String TrainTest = args[2];

			Settings settings = null;
			int partIdx = 0;
			if (args.length == 4) {
				partIdx = Integer.parseInt(args[3]);
				settings = new Settings(r, TrainTest, d, "", "." + partIdx);
			} else {
				settings = new Settings(r, TrainTest, d, "", "");
			}
			DetectTargets trgs = new DetectTargets(settings);
			trgs.createTargetListFromTrain();
			trgs.assign();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
