/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ import java.io.ByteArrayInputStream;
/*  4:   */ import java.io.ByteArrayOutputStream;
/*  5:   */ import java.io.ObjectInputStream;
/*  6:   */ import java.io.ObjectOutputStream;
/*  7:   */ import java.io.PrintStream;
/*  8:   */ 
/*  9:   */ public class ObjectCloner
/* 10:   */ {
/* 11:   */   public static Object deepCopy(Object oldObj)
/* 12:   */     throws Exception
/* 13:   */   {
/* 14:15 */     ObjectOutputStream oos = null;
/* 15:16 */     ObjectInputStream ois = null;
/* 16:   */     try
/* 17:   */     {
/* 18:19 */       ByteArrayOutputStream bos = 
/* 19:20 */         new ByteArrayOutputStream();
/* 20:21 */       oos = new ObjectOutputStream(bos);
/* 21:   */       
/* 22:23 */       oos.writeObject(oldObj);
/* 23:24 */       oos.flush();
/* 24:25 */       ByteArrayInputStream bin = 
/* 25:26 */         new ByteArrayInputStream(bos.toByteArray());
/* 26:27 */       ois = new ObjectInputStream(bin);
/* 27:   */       
/* 28:29 */       return ois.readObject();
/* 29:   */     }
/* 30:   */     catch (Exception e)
/* 31:   */     {
/* 32:33 */       System.out.println("Exception in ObjectCloner = " + e);
/* 33:34 */       throw e;
/* 34:   */     }
/* 35:   */     finally
/* 36:   */     {
/* 37:38 */       oos.close();
/* 38:39 */       ois.close();
/* 39:   */     }
/* 40:   */   }
/* 41:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.ObjectCloner
 * JD-Core Version:    0.7.0.1
 */