/*   1:    */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*   2:    */ 
/*   3:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
/*   4:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
/*   5:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
/*   6:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
/*   7:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
/*   8:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
/*   9:    */ import java.io.FileOutputStream;
/*  10:    */ import java.io.PrintStream;
/*  11:    */ import java.util.ArrayList;
/*  12:    */ import java.util.HashMap;
/*  13:    */ import java.util.LinkedHashMap;
/*  14:    */ import java.util.StringTokenizer;
/*  15:    */ 
/*  16:    */ public class Utils
/*  17:    */ {
/*  18: 17 */   private static String delim = " ,.!'";
/*  19:    */   
/*  20:    */   public static ABSA15Reviews removeOp(ABSA15Reviews orig)
/*  21:    */     throws Exception
/*  22:    */   {
/*  23: 22 */     for (int i = 0; i < orig.getReviews().size(); i++)
/*  24:    */     {
/*  25: 24 */       ABSA15Review o = (ABSA15Review)orig.getReviews().get(i);
/*  26: 26 */       for (int j = 0; j < o.getSentences().getSentenceList().size(); j++)
/*  27:    */       {
/*  28: 28 */         ABSA15Sentence sent = (ABSA15Sentence)o.getSentences().getSentenceList().get(j);
/*  29: 29 */         sent.setOp(null);
/*  30:    */       }
/*  31:    */     }
/*  32: 34 */     return orig;
/*  33:    */   }
/*  34:    */   
/*  35:    */   public static ABSA15Reviews norm(ABSA15Reviews orig)
/*  36:    */   {
/*  37: 40 */     for (int i = 0; i < orig.getReviews().size(); i++)
/*  38:    */     {
/*  39: 42 */       ABSA15Sentences sentences = ((ABSA15Review)orig.getReviews().get(i)).getSentences();
/*  40: 44 */       for (int j = 0; j < sentences.getSentenceList().size(); j++)
/*  41:    */       {
/*  42: 46 */         ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/*  43: 47 */         if ((sent.getOp() != null) && (sent.getOp().getOpiList().size() == 0)) {
/*  44: 49 */           sent.setOp(null);
/*  45:    */         }
/*  46:    */       }
/*  47:    */     }
/*  48: 54 */     return orig;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public static ABSA15Reviews removePol(ABSA15Reviews orig)
/*  52:    */   {
/*  53: 60 */     for (int i = 0; i < orig.getReviews().size(); i++)
/*  54:    */     {
/*  55: 62 */       ABSA15Sentences sentences = ((ABSA15Review)orig.getReviews().get(i)).getSentences();
/*  56: 64 */       for (int j = 0; j < sentences.getSentenceList().size(); j++)
/*  57:    */       {
/*  58: 66 */         ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/*  59: 68 */         if (sent.getOp() != null) {
/*  60: 70 */           for (int k = 0; k < sent.getOp().getOpiList().size(); k++) {
/*  61: 72 */             ((ABSA15Opinion)sent.getOp().getOpiList().get(k)).setPolarity("");
/*  62:    */           }
/*  63:    */         }
/*  64:    */       }
/*  65:    */     }
/*  66: 78 */     return orig;
/*  67:    */   }
/*  68:    */   
/*  69:    */   public static void createFeatureVectors(ABSA15Reviews revs, Settings sts, ArrayList<TokenFreq> tokensFeatArray, int mod, int stage, String[] out)
/*  70:    */   {
/*  71:    */     try
/*  72:    */     {
/*  73: 85 */       AspectsMapping AM = sts.getAM();
/*  74:    */       
/*  75: 87 */       HashMap<String, Integer> usedFeats = new HashMap();
/*  76: 88 */       for (int i = 0; i < tokensFeatArray.size(); i++) {
/*  77: 90 */         usedFeats.put(((TokenFreq)tokensFeatArray.get(i)).getToken(), Integer.valueOf(i));
/*  78:    */       }
/*  79: 93 */       FileOutputStream fOSAsp = null;
/*  80: 94 */       FileOutputStream fOSPol = null;
/*  81: 96 */       if (mod == 1) {
/*  82: 98 */         if (stage == 1)
/*  83:    */         {
/*  84:100 */           fOSAsp = new FileOutputStream(out[0]);
/*  85:101 */           fOSPol = new FileOutputStream(out[1]);
/*  86:    */         }
/*  87:    */       }
/*  88:105 */       if (mod == 2)
/*  89:    */       {
/*  90:107 */         if (stage == 1) {
/*  91:109 */           fOSAsp = new FileOutputStream(out[0]);
/*  92:    */         }
/*  93:112 */         if (stage == 2) {
/*  94:114 */           fOSPol = new FileOutputStream(out[0]);
/*  95:    */         }
/*  96:    */       }
/*  97:119 */       for (int i = 0; i < revs.getReviews().size(); i++)
/*  98:    */       {
/*  99:121 */         ABSA15Review r = (ABSA15Review)revs.getReviews().get(i);
/* 100:122 */         ABSA15Sentences sentences = r.getSentences();
/* 101:124 */         for (int j = 0; j < sentences.getSentenceList().size(); j++)
/* 102:    */         {
/* 103:126 */           ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/* 104:127 */           String text = sent.getText();
/* 105:    */           
/* 106:129 */           ArrayList<String> tokenization = new ArrayList();
/* 107:131 */           if (sent.getOOS() == null)
/* 108:    */           {
/* 109:133 */             StringTokenizer st = new StringTokenizer(text, delim, true);
/* 110:    */             
/* 111:135 */             int[] features = new int[tokensFeatArray.size()];
/* 112:136 */             while (st.hasMoreTokens())
/* 113:    */             {
/* 114:138 */               String token = st.nextToken();
/* 115:139 */               token = token.toLowerCase();
/* 116:140 */               tokenization.add(token);
/* 117:142 */               if (usedFeats.get(token) != null)
/* 118:    */               {
/* 119:144 */                 int featIdx = ((Integer)usedFeats.get(token)).intValue();
/* 120:145 */                 features[featIdx] += 1;
/* 121:    */               }
/* 122:    */             }
/* 123:152 */             String featStr = "";
/* 124:153 */             int featIdx = 0;
/* 125:155 */             for (featIdx = 0; featIdx < features.length; featIdx++)
/* 126:    */             {
/* 127:157 */               featStr = featStr + (featIdx + 1) + ":" + features[featIdx];
/* 128:159 */               if (featIdx != features.length - 1) {
/* 129:161 */                 featStr = featStr + " ";
/* 130:    */               }
/* 131:    */             }
/* 132:166 */             if (mod == 1) {
/* 133:168 */               if (stage == 1) {
/* 134:171 */                 for (int k = 0; k < sent.getOp().getOpiList().size(); k++)
/* 135:    */                 {
/* 136:173 */                   ABSA15Opinion o = (ABSA15Opinion)sent.getOp().getOpiList().get(k);
/* 137:174 */                   String cat = o.getCategory();
/* 138:    */                   
/* 139:    */ 
/* 140:177 */                   Integer intLabel = (Integer)AM.getLabeltoNum().get(cat);
/* 141:179 */                   if (intLabel != null)
/* 142:    */                   {
/* 143:181 */                     int label = ((Integer)AM.getLabeltoNum().get(cat)).intValue();
/* 144:182 */                     String str = label + " " + featStr + "\n";
/* 145:    */                     
/* 146:184 */                     fOSAsp.write(str.getBytes());
/* 147:185 */                     fOSAsp.flush();
/* 148:    */                   }
/* 149:    */                   else
/* 150:    */                   {
/* 151:190 */                     System.err.println(r.getRid() + " " + j + " " + cat);
/* 152:    */                   }
/* 153:195 */                   if ((o.getTarget() != null) && (!o.getTarget().equals("NULL")))
/* 154:    */                   {
/* 155:197 */                     int a = 0;
/* 156:198 */                     int b = 0;
/* 157:199 */                     int pre_len = 0;
/* 158:201 */                     for (int t = 0; t < tokenization.size(); t++)
/* 159:    */                     {
/* 160:203 */                       String tok = (String)tokenization.get(t);
/* 161:    */                       
/* 162:205 */                       a += pre_len;
/* 163:206 */                       b = a + tok.length();
/* 164:208 */                       if (a >= o.getFrom()) {
/* 165:208 */                         o.getTo();
/* 166:    */                       }
/* 167:213 */                       pre_len = tok.length();
/* 168:    */                     }
/* 169:    */                   }
/* 170:220 */                   Integer pol = (Integer)PolarityLabelsMapping.map.get(o.getPolarity());
/* 171:222 */                   if (pol != null)
/* 172:    */                   {
/* 173:224 */                     String str2 = 
/* 174:225 */                       pol.intValue() + " " + 
/* 175:226 */                       featStr + 
/* 176:227 */                       " " + (featIdx + 1) + ":" + AM.getLabeltoNum().get(cat) + 
/* 177:228 */                       "\n";
/* 178:    */                     
/* 179:230 */                     fOSPol.write(str2.getBytes());
/* 180:231 */                     fOSPol.flush();
/* 181:    */                   }
/* 182:    */                   else
/* 183:    */                   {
/* 184:236 */                     System.err.println(r.getRid() + " " + j + " " + pol);
/* 185:    */                   }
/* 186:    */                 }
/* 187:    */               }
/* 188:    */             }
/* 189:250 */             if (mod == 2)
/* 190:    */             {
/* 191:252 */               if (stage == 1)
/* 192:    */               {
/* 193:256 */                 String str = "0 " + featStr + "\n";
/* 194:257 */                 fOSAsp.write(str.getBytes());
/* 195:258 */                 fOSAsp.flush();
/* 196:    */               }
/* 197:261 */               if (stage == 2) {
/* 198:264 */                 for (int k = 0; k < sent.getOp().getOpiList().size(); k++)
/* 199:    */                 {
/* 200:266 */                   ABSA15Opinion o = (ABSA15Opinion)sent.getOp().getOpiList().get(k);
/* 201:267 */                   String cat = o.getCategory();
/* 202:270 */                   if (AM.getLabeltoNum().get(cat) == null) {
/* 203:272 */                     System.err.println(cat);
/* 204:    */                   }
/* 205:277 */                   String str2 = 
/* 206:278 */                     "0 " + 
/* 207:279 */                     featStr + 
/* 208:280 */                     " " + (featIdx + 1) + ":" + AM.getLabeltoNum().get(cat) + 
/* 209:281 */                     "\n";
/* 210:    */                   
/* 211:283 */                   fOSPol.write(str2.getBytes());
/* 212:284 */                   fOSPol.flush();
/* 213:    */                 }
/* 214:    */               }
/* 215:    */             }
/* 216:    */           }
/* 217:    */         }
/* 218:    */       }
/* 219:292 */       if (fOSPol != null) {
/* 220:294 */         fOSPol.close();
/* 221:    */       }
/* 222:297 */       if (fOSAsp != null) {
/* 223:299 */         fOSAsp.close();
/* 224:    */       }
/* 225:    */     }
/* 226:    */     catch (Exception e)
/* 227:    */     {
/* 228:305 */       e.printStackTrace();
/* 229:    */     }
/* 230:    */   }
/* 231:    */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.Utils
 * JD-Core Version:    0.7.0.1
 */