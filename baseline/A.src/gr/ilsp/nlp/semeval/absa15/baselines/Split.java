/*   1:    */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*   2:    */ 
/*   3:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
/*   4:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
/*   5:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
/*   6:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
/*   7:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
/*   8:    */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
/*   9:    */ import gr.ilsp.nlp.semeval.absa15.xml.SemEval2015Marshaller;
/*  10:    */ import java.io.File;
/*  11:    */ import java.io.PrintStream;
/*  12:    */ import java.util.ArrayList;
/*  13:    */ import java.util.Collections;
/*  14:    */ import java.util.List;
/*  15:    */ import java.util.Random;
/*  16:    */ import javax.xml.bind.Marshaller;
/*  17:    */ 
/*  18:    */ public class Split
/*  19:    */ {
/*  20:    */   private ABSA15Unmarshaller unm;
/*  21:    */   private SemEval2015Marshaller marhshaller;
/*  22:    */   private ABSA15Reviews revs;
/*  23:    */   private Marshaller jaxbMarshaller;
/*  24:    */   private Settings settings;
/*  25:    */   private int[] splitIdxs;
/*  26:    */   private int opiNum;
/*  27:    */   private int partOpiNum;
/*  28:    */   private int parts;
/*  29: 29 */   private String suffix = "";
/*  30:    */   
/*  31:    */   public Split(Settings sts)
/*  32:    */   {
/*  33: 33 */     this.unm = new ABSA15Unmarshaller();
/*  34: 34 */     this.marhshaller = new SemEval2015Marshaller();
/*  35: 35 */     this.jaxbMarshaller = this.marhshaller.getJaxbMarshaller();
/*  36: 36 */     this.settings = sts;
/*  37:    */   }
/*  38:    */   
/*  39:    */   public void split(int shuffle, int parts)
/*  40:    */   {
/*  41:    */     try
/*  42:    */     {
/*  43: 43 */       this.parts = parts;
/*  44: 44 */       String source = this.settings.getInputFullPath();
/*  45:    */       
/*  46:    */ 
/*  47: 47 */       this.revs = this.unm.read(source);
/*  48: 50 */       if (shuffle == 1)
/*  49:    */       {
/*  50: 52 */         System.err.println("Shuffling reviews.");
/*  51: 53 */         Collections.shuffle(this.revs.getReviews(), new Random(1981L));
/*  52:    */       }
/*  53: 56 */       this.splitIdxs = new int[parts + 1];
/*  54: 57 */       this.splitIdxs[0] = -1;
/*  55: 58 */       this.splitIdxs[parts] = (this.revs.getReviews().size() - 1);
/*  56:    */       
/*  57:    */ 
/*  58: 61 */       this.opiNum = 0;
/*  59: 62 */       for (int i = 0; i < this.revs.getReviews().size(); i++)
/*  60:    */       {
/*  61: 64 */         ABSA15Sentences sentences = ((ABSA15Review)this.revs.getReviews().get(i)).getSentences();
/*  62: 66 */         for (int j = 0; j < sentences.getSentenceList().size(); j++)
/*  63:    */         {
/*  64: 68 */           ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/*  65: 70 */           if (sent.getOp() != null) {
/*  66: 72 */             this.opiNum += sent.getOp().getOpiList().size();
/*  67:    */           }
/*  68:    */         }
/*  69:    */       }
/*  70: 78 */       int partSize = this.opiNum / parts;
/*  71:    */       
/*  72:    */ 
/*  73: 81 */       int idx = 1;
/*  74: 82 */       int sum = 0;
/*  75: 84 */       for (int i = 0; i < this.revs.getReviews().size(); i++)
/*  76:    */       {
/*  77: 87 */         ABSA15Sentences sentences = ((ABSA15Review)this.revs.getReviews().get(i)).getSentences();
/*  78: 88 */         for (int j = 0; j < sentences.getSentenceList().size(); j++)
/*  79:    */         {
/*  80: 90 */           ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/*  81: 92 */           if (sent.getOp() != null) {
/*  82: 94 */             sum += sent.getOp().getOpiList().size();
/*  83:    */           }
/*  84:    */         }
/*  85: 98 */         if (sum >= partSize)
/*  86:    */         {
/*  87:100 */           this.splitIdxs[idx] = i;
/*  88:101 */           sum = 0;
/*  89:102 */           idx++;
/*  90:    */         }
/*  91:105 */         if (idx == parts) {
/*  92:107 */           i = this.revs.getReviews().size();
/*  93:    */         }
/*  94:    */       }
/*  95:    */     }
/*  96:    */     catch (Exception e)
/*  97:    */     {
/*  98:114 */       e.printStackTrace();
/*  99:    */     }
/* 100:    */   }
/* 101:    */   
/* 102:    */   private void createAll(int partIdx)
/* 103:    */   {
/* 104:120 */     if (partIdx == -1) {
/* 105:122 */       for (int i = 0; i < this.parts; i++)
/* 106:    */       {
/* 107:124 */         System.err.println("Part: " + (i + 1));
/* 108:    */         
/* 109:126 */         this.suffix = ("." + i);
/* 110:127 */         createFiles(i);
/* 111:    */       }
/* 112:130 */     } else if (partIdx != -1) {
/* 113:132 */       createFiles(partIdx);
/* 114:    */     }
/* 115:    */   }
/* 116:    */   
/* 117:    */   private void createFiles(int partIdx)
/* 118:    */   {
/* 119:    */     try
/* 120:    */     {
/* 121:140 */       String Tr = this.settings.getTrain() + this.suffix;
/* 122:141 */       String TeG = this.settings.getTeGldAll() + this.suffix;
/* 123:    */       
/* 124:143 */       String TeC = this.settings.getTeCln() + this.suffix;
/* 125:144 */       String TeGoAs = this.settings.getTeGldAspTrg() + this.suffix;
/* 126:    */       
/* 127:    */ 
/* 128:147 */       int partOpiNum = 0;
/* 129:148 */       List<ABSA15Review> te = new ArrayList();
/* 130:149 */       for (int i = this.splitIdxs[partIdx] + 1; i <= this.splitIdxs[(partIdx + 1)]; i++)
/* 131:    */       {
/* 132:151 */         ABSA15Review c = (ABSA15Review)ObjectCloner.deepCopy(this.revs.getReviews().get(i));
/* 133:152 */         te.add(c);
/* 134:    */         
/* 135:154 */         ABSA15Sentences sentences = ((ABSA15Review)this.revs.getReviews().get(i)).getSentences();
/* 136:155 */         for (int j = 0; j < sentences.getSentenceList().size(); j++)
/* 137:    */         {
/* 138:157 */           ABSA15Sentence sent = (ABSA15Sentence)sentences.getSentenceList().get(j);
/* 139:159 */           if (sent.getOp() != null) {
/* 140:161 */             partOpiNum += sent.getOp().getOpiList().size();
/* 141:    */           }
/* 142:    */         }
/* 143:    */       }
/* 144:168 */       List<ABSA15Review> tr = (List)ObjectCloner.deepCopy(this.revs.getReviews());
/* 145:    */       
/* 146:170 */       tr.removeAll(te);
/* 147:    */       
/* 148:    */ 
/* 149:173 */       File output = null;
/* 150:174 */       ABSA15Reviews chunk = null;
/* 151:175 */       System.err.println("All #reviews=" + this.revs.getReviews().size() + " #Opinions=" + this.opiNum);
/* 152:    */       
/* 153:    */ 
/* 154:178 */       chunk = new ABSA15Reviews();
/* 155:179 */       chunk.getReviews().addAll(tr);
/* 156:180 */       output = new File(Tr);
/* 157:181 */       this.jaxbMarshaller.marshal(chunk, output);
/* 158:182 */       System.err.println("train #reviews=" + tr.size() + " #Opinions=" + (this.opiNum - partOpiNum));
/* 159:    */       
/* 160:    */ 
/* 161:185 */       chunk = new ABSA15Reviews();
/* 162:186 */       chunk.getReviews().addAll((List)ObjectCloner.deepCopy(te));
/* 163:187 */       output = new File(TeG);
/* 164:188 */       this.jaxbMarshaller.marshal(chunk, output);
/* 165:    */       
/* 166:    */ 
/* 167:191 */       chunk = new ABSA15Reviews();
/* 168:192 */       chunk.getReviews().addAll((List)ObjectCloner.deepCopy(te));
/* 169:193 */       chunk = Utils.removeOp(chunk);
/* 170:194 */       output = new File(TeC);
/* 171:195 */       this.jaxbMarshaller.marshal(chunk, output);
/* 172:    */       
/* 173:    */ 
/* 174:198 */       chunk = new ABSA15Reviews();
/* 175:199 */       chunk.getReviews().addAll((List)ObjectCloner.deepCopy(te));
/* 176:200 */       chunk = Utils.removePol(chunk);
/* 177:201 */       output = new File(TeGoAs);
/* 178:202 */       this.jaxbMarshaller.marshal(chunk, output);
/* 179:    */       
/* 180:204 */       System.err.println("test #reviews=" + te.size() + " #Opinions=" + partOpiNum);
/* 181:    */     }
/* 182:    */     catch (Exception e)
/* 183:    */     {
/* 184:213 */       e.printStackTrace();
/* 185:    */     }
/* 186:    */   }
/* 187:    */   
/* 188:    */   public static void main(String[] args)
/* 189:    */   {
/* 190:220 */     int shuffle = Integer.parseInt(args[0]);
/* 191:221 */     String r = args[1];
/* 192:222 */     String TrainTest = args[2];
/* 193:223 */     String input = args[3];
/* 194:224 */     int parts = Integer.parseInt(args[4]);
/* 195:    */     
/* 196:226 */     Settings settings = new Settings(r, TrainTest, "", input, "");
/* 197:227 */     int partIdx = 0;
/* 198:229 */     if (args.length == 6) {
/* 199:231 */       partIdx = Integer.parseInt(args[5]);
/* 200:    */     } else {
/* 201:235 */       partIdx = -1;
/* 202:    */     }
/* 203:238 */     Split split = new Split(settings);
/* 204:239 */     split.split(shuffle, parts);
/* 205:240 */     split.createAll(partIdx);
/* 206:    */     
/* 207:242 */     System.err.println("Splitting is completed...");
/* 208:    */   }
/* 209:    */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.Split
 * JD-Core Version:    0.7.0.1
 */