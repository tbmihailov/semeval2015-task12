package gr.ilsp.nlp.semeval.absa15.baselines;

import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;

public class CreateVecs
{
  public void doit(Settings settings, int mode, int stage)
  {
    try
    {
      ArrayList<TokenFreq> array = new ArrayList();
      String line = "";
      

      String words = settings.getWords();
      BufferedReader reader = new BufferedReader(new FileReader(words));
      while ((line = reader.readLine()) != null)
      {
        TokenFreq t = new TokenFreq(line, 1);
        array.add(t);
      }
      System.err.println("Create and Write Feature vectors...");
      

      ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
      ABSA15Reviews revs = null;
      if (mode == 1)
      {
        String[] out = new String[2];
        if (stage == 1)
        {
          out[0] = settings.getTrFeatFileA();
          out[1] = settings.getTrFeatFileP();
        }
        revs = unm.read(settings.getTrain());
        Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
      }
      if (mode == 2)
      {
        String[] out = new String[1];
        if (stage == 1)
        {
          out[0] = settings.getTeFeatFileA();
          revs = unm.read(settings.getTeCln());
          Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
        }
        if (stage == 2)
        {
          revs = unm.read(settings.getTeGldAspTrg());
          out[0] = settings.getTeFeatFileP4Gold();
          Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
          
          revs = unm.read(settings.getTeCln_PrdAspTrg());
          out[0] = settings.getTeFeatFileP4Pred();
          Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
        }
      }
      reader.close();
      System.err.println("Finished!");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void doit_mallet(Settings settings, int mode, int stage)
  {
    try
    {
      ArrayList<TokenFreq> array = new ArrayList();
      String line = "";
      

      String words = settings.getWords();
      BufferedReader reader = new BufferedReader(new FileReader(words));
      while ((line = reader.readLine()) != null)
      {
        TokenFreq t = new TokenFreq(line, 1);
        array.add(t);
      }
      System.err.println("Create and Write Feature vectors...");
      

      ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
      ABSA15Reviews revs = null;
      if (mode == 1)
      {
        String[] out = new String[2];
        if (stage == 1)
        {
          out[0] = settings.getTrFeatFileA();
          out[1] = settings.getTrFeatFileP();
        }
        revs = unm.read(settings.getTrain());
        Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
      }
      if (mode == 2)
      {
        String[] out = new String[1];
        if (stage == 1)
        {
          out[0] = settings.getTeFeatFileA();
          revs = unm.read(settings.getTeCln());
          Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
        }
        if (stage == 2)
        {
          revs = unm.read(settings.getTeGldAspTrg());
          out[0] = settings.getTeFeatFileP4Gold();
          Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
          
          revs = unm.read(settings.getTeCln_PrdAspTrg());
          out[0] = settings.getTeFeatFileP4Pred();
          Utils.createFeatureVectors(revs, settings, array, mode, stage, out);
        }
      }
      reader.close();
      System.err.println("Finished!");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public static void main(String[] args)
  {
    String d = args[0];
    String r = args[1];
    String TrainTest = args[2];
    int mode = Integer.parseInt(args[3]);
    int stage = Integer.parseInt(args[4]);
    
    Settings settings = null;
    int partIdx = 0;
    if (args.length == 6)
    {
      partIdx = Integer.parseInt(args[5]);
      settings = new Settings(r, TrainTest, d, "", "." + partIdx);
    }
    else
    {
      settings = new Settings(r, TrainTest, d, "", "");
    }
    CreateVecs cv = new CreateVecs();
    cv.doit(settings, mode, stage);
  }
}
