/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ public class CountAndCheckTester
/*  4:   */ {
/*  5:   */   public static void main(String[] args)
/*  6:   */   {
/*  7: 7 */     String xml = "";
/*  8: 8 */     String xsd = "C:/Users/galanisd/Desktop/Dimitris/ILSPDocs/PrjSemEval15Task12WrkShop/XmlSchema/ABSA15Mod.xsd";
/*  9:   */     
/* 10:   */ 
/* 11:   */ 
/* 12:12 */     xml = "C:/Users/galanisd/Desktop/Dimitris/ILSPDocs/PrjSemEval15Task12WrkShop/ABSAData/14_11_03Release/CopyABSA-15_Laptops_Train_Data.xml";
/* 13:13 */     CountAndCheck.check(xml, xsd, "lapt");
/* 14:   */     
/* 15:   */ 
/* 16:   */ 
/* 17:   */ 
/* 18:   */ 
/* 19:19 */     xml = "C:/Users/galanisd/Desktop/Dimitris/ILSPDocs/PrjSemEval15Task12WrkShop/ABSAData/14_11_03Release/CopyABSA-15_Restaurants_Train_Final.xml";
/* 20:20 */     CountAndCheck.check(xml, xsd, "rest");
/* 21:   */   }
/* 22:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.CountAndCheckTester
 * JD-Core Version:    0.7.0.1
 */