package gr.ilsp.nlp.semeval.absa15.baselines;

import java.util.HashMap;

public class AspectsMapping
{
  private HashMap<Integer, String> numtoLabel = null;
  private HashMap<String, Integer> LabeltoNum = null;
  
  public AspectsMapping()
  {
    this.numtoLabel = new HashMap();
    this.LabeltoNum = new HashMap();
  }
  
  public void load(Aspects Asp)
  {
    try
    {
      int c = 1;
      for (int i = 0; i < Asp.getA().length; i++) {
        for (int j = 0; j < Asp.getB().length; j++)
        {
          String cat = Asp.getA()[i] + "#" + Asp.getB()[j];
          this.LabeltoNum.put(cat, Integer.valueOf(c));
          this.numtoLabel.put(Integer.valueOf(c), cat);
          

          c++;
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public HashMap<String, Integer> getLabeltoNum()
  {
    return this.LabeltoNum;
  }
  
  public void setLabeltoNum(HashMap<String, Integer> labeltoNum)
  {
    this.LabeltoNum = labeltoNum;
  }
  
  public HashMap<Integer, String> getNumtoLabel()
  {
    return this.numtoLabel;
  }
  
  public void setNumtoLabel(HashMap<Integer, String> numtoLabel)
  {
    this.numtoLabel = numtoLabel;
  }
}
