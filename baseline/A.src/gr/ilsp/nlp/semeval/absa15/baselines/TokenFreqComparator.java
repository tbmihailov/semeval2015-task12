/*  1:   */ package gr.ilsp.nlp.semeval.absa15.baselines;
/*  2:   */ 
/*  3:   */ import java.util.Comparator;
/*  4:   */ 
/*  5:   */ public class TokenFreqComparator
/*  6:   */   implements Comparator<TokenFreq>
/*  7:   */ {
/*  8:   */   public int compare(TokenFreq o1, TokenFreq o2)
/*  9:   */   {
/* 10:14 */     return -(o1.getF() - o2.getF());
/* 11:   */   }
/* 12:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.baselines.TokenFreqComparator
 * JD-Core Version:    0.7.0.1
 */