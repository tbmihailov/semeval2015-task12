/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import java.io.Serializable;
/*  4:   */ import javax.xml.bind.annotation.XmlAttribute;
/*  5:   */ import javax.xml.bind.annotation.XmlElement;
/*  6:   */ import javax.xml.bind.annotation.XmlRootElement;
/*  7:   */ import javax.xml.bind.annotation.XmlType;
/*  8:   */ 
/*  9:   */ @XmlType(propOrder={"text", "op"})
/* 10:   */ @XmlRootElement(name="sentence")
/* 11:   */ public class ABSA15Sentence
/* 12:   */   implements Serializable
/* 13:   */ {
/* 14:   */   private String id;
/* 15:   */   private String text;
/* 16:   */   private String OOS;
/* 17:   */   private ABSA15OpiSet op;
/* 18:   */   
/* 19:   */   public ABSA15Sentence()
/* 20:   */   {
/* 21:21 */     this.id = "";
/* 22:22 */     this.text = "";
/* 23:23 */     this.op = new ABSA15OpiSet();
/* 24:   */   }
/* 25:   */   
/* 26:   */   public String getId()
/* 27:   */   {
/* 28:27 */     return this.id;
/* 29:   */   }
/* 30:   */   
/* 31:   */   @XmlAttribute(name="id")
/* 32:   */   public void setId(String id)
/* 33:   */   {
/* 34:32 */     this.id = id;
/* 35:   */   }
/* 36:   */   
/* 37:   */   public String getText()
/* 38:   */   {
/* 39:36 */     return this.text;
/* 40:   */   }
/* 41:   */   
/* 42:   */   @XmlElement(name="text")
/* 43:   */   public void setText(String text)
/* 44:   */   {
/* 45:41 */     this.text = text;
/* 46:   */   }
/* 47:   */   
/* 48:   */   public ABSA15OpiSet getOp()
/* 49:   */   {
/* 50:45 */     return this.op;
/* 51:   */   }
/* 52:   */   
/* 53:   */   @XmlElement(name="Opinions")
/* 54:   */   public void setOp(ABSA15OpiSet op)
/* 55:   */   {
/* 56:50 */     this.op = op;
/* 57:   */   }
/* 58:   */   
/* 59:   */   public String getOOS()
/* 60:   */   {
/* 61:54 */     return this.OOS;
/* 62:   */   }
/* 63:   */   
/* 64:   */   @XmlAttribute(name="OutOfScope")
/* 65:   */   public void setOOS(String oOS)
/* 66:   */   {
/* 67:59 */     this.OOS = oOS;
/* 68:   */   }
/* 69:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence
 * JD-Core Version:    0.7.0.1
 */