package gr.ilsp.nlp.semeval.absa15.xml;

public class ABSA15Constants
{
  public static final String REVIEWS = "Reviews";
  public static final String REVIEW = "Review";
  public static final String RID = "rid";
  public static final String SENTENCES = "sentences";
  public static final String SENTENCE = "sentence";
  public static final String ID = "id";
  public static final String TEXT = "text";
  public static final String OPINIONS = "Opinions";
  public static final String OPINIONTarget = "Opinion";
  public static final String TRG = "target";
  public static final String POL = "polarity";
  public static final String CAT = "category";
  public static final String FROM = "from";
  public static final String To = "to";
  public static final String NC = "NotConf";
  public static final String OutOfScope = "OutOfScope";
}


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Constants
 * JD-Core Version:    0.7.0.1
 */