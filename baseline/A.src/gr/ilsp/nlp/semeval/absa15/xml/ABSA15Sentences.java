package gr.ilsp.nlp.semeval.absa15.xml;

import gr.ilsp.nlp.semeval.absa15.xml.ABSA15SentenceComp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="sentences")
public class ABSA15Sentences
  implements Serializable
{
  private ArrayList<ABSA15Sentence> sentenceList;
  
  public ABSA15Sentences()
  {
    this.sentenceList = new ArrayList();
  }
  
  public ArrayList<ABSA15Sentence> getSentenceList()
  {
    return this.sentenceList;
  }
  
  @XmlElement(name="sentence")
  public void setSentenceList(ArrayList<ABSA15Sentence> sentenceList)
  {
    this.sentenceList = sentenceList;
  }
  
  //public void sort()
  //{
  //  Collections.sort(this.sentenceList, new ABSA15SentenceComp());
  //}
}
