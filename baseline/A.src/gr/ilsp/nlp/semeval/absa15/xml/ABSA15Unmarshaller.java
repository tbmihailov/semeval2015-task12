/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import java.io.FileReader;
/*  4:   */ import javax.xml.bind.JAXBContext;
/*  5:   */ import javax.xml.bind.Unmarshaller;
/*  6:   */ 
/*  7:   */ public class ABSA15Unmarshaller
/*  8:   */ {
/*  9:11 */   private Unmarshaller um = null;
/* 10:12 */   private JAXBContext context = null;
/* 11:   */   
/* 12:   */   public ABSA15Unmarshaller()
/* 13:   */   {
/* 14:   */     try
/* 15:   */     {
/* 16:18 */       this.context = JAXBContext.newInstance(new Class[] { ABSA15Reviews.class });
/* 17:19 */       this.um = this.context.createUnmarshaller();
/* 18:   */     }
/* 19:   */     catch (Exception e)
/* 20:   */     {
/* 21:23 */       e.printStackTrace();
/* 22:   */     }
/* 23:   */   }
/* 24:   */   
/* 25:   */   public ABSA15Reviews read(String xml)
/* 26:   */     throws Exception
/* 27:   */   {
/* 28:29 */     return (ABSA15Reviews)this.um.unmarshal(new FileReader(xml));
/* 29:   */   }
/* 30:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller
 * JD-Core Version:    0.7.0.1
 */