/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import javax.xml.bind.annotation.XmlRootElement;
/*  4:   */ import javax.xml.bind.annotation.XmlValue;
/*  5:   */ 
/*  6:   */ @XmlRootElement(name="text")
/*  7:   */ public class ABSA15Text
/*  8:   */ {
/*  9:   */   private String text;
/* 10:   */   
/* 11:   */   public String getText()
/* 12:   */   {
/* 13:11 */     return this.text;
/* 14:   */   }
/* 15:   */   
/* 16:   */   @XmlValue
/* 17:   */   public void setText(String text)
/* 18:   */   {
/* 19:16 */     this.text = text;
/* 20:   */   }
/* 21:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Text
 * JD-Core Version:    0.7.0.1
 */