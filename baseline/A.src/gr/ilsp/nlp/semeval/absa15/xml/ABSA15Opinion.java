/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import java.io.Serializable;
/*  4:   */ import javax.xml.bind.annotation.XmlAttribute;
/*  5:   */ import javax.xml.bind.annotation.XmlRootElement;
/*  6:   */ import javax.xml.bind.annotation.XmlType;
/*  7:   */ 
/*  8:   */ @XmlType(propOrder={"target", "category", "polarity", "from", "to"})
/*  9:   */ @XmlRootElement(name="Opinions")
/* 10:   */ public class ABSA15Opinion
/* 11:   */   implements Serializable
/* 12:   */ {
/* 13:   */   private int from;
/* 14:   */   private int to;
/* 15:   */   private String target;
/* 16:   */   private String category;
/* 17:   */   private String polarity;
/* 18:   */   private String NotConf;
/* 19:   */   
/* 20:   */   public String getTarget()
/* 21:   */   {
/* 22:22 */     return this.target;
/* 23:   */   }
/* 24:   */   
/* 25:   */   @XmlAttribute(name="target")
/* 26:   */   public void setTarget(String target)
/* 27:   */   {
/* 28:27 */     this.target = target.replaceAll("\"", "&quot;");
/* 29:   */   }
/* 30:   */   
/* 31:   */   public String getCategory()
/* 32:   */   {
/* 33:30 */     return this.category;
/* 34:   */   }
/* 35:   */   
/* 36:   */   @XmlAttribute(name="category")
/* 37:   */   public void setCategory(String category)
/* 38:   */   {
/* 39:35 */     this.category = category;
/* 40:   */   }
/* 41:   */   
/* 42:   */   public String getPolarity()
/* 43:   */   {
/* 44:38 */     return this.polarity;
/* 45:   */   }
/* 46:   */   
/* 47:   */   @XmlAttribute(name="polarity")
/* 48:   */   public void setPolarity(String polarity)
/* 49:   */   {
/* 50:43 */     this.polarity = polarity;
/* 51:   */   }
/* 52:   */   
/* 53:   */   public int getFrom()
/* 54:   */   {
/* 55:46 */     return this.from;
/* 56:   */   }
/* 57:   */   
/* 58:   */   @XmlAttribute(name="from")
/* 59:   */   public void setFrom(int from)
/* 60:   */   {
/* 61:51 */     this.from = from;
/* 62:   */   }
/* 63:   */   
/* 64:   */   public int getTo()
/* 65:   */   {
/* 66:55 */     return this.to;
/* 67:   */   }
/* 68:   */   
/* 69:   */   @XmlAttribute(name="to")
/* 70:   */   public void setTo(int to)
/* 71:   */   {
/* 72:60 */     this.to = to;
/* 73:   */   }
/* 74:   */   
/* 75:   */   public String getNotConf()
/* 76:   */   {
/* 77:64 */     return this.NotConf;
/* 78:   */   }
/* 79:   */   
/* 80:   */   @XmlAttribute(name="NotConf")
/* 81:   */   public void setNotConf(String notConf)
/* 82:   */   {
/* 83:69 */     this.NotConf = notConf;
/* 84:   */   }
/* 85:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion
 * JD-Core Version:    0.7.0.1
 */