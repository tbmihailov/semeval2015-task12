/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import com.sun.xml.internal.bind.marshaller.CharacterEscapeHandler;
/*  4:   */ import java.io.IOException;
/*  5:   */ import java.io.Writer;
/*  6:   */ import javax.xml.bind.JAXBContext;
/*  7:   */ import javax.xml.bind.Marshaller;
/*  8:   */ 
/*  9:   */ public class SemEval2015Marshaller
/* 10:   */ {
/* 11:   */   private Marshaller jaxbMarshaller;
/* 12:   */   
/* 13:   */   public SemEval2015Marshaller()
/* 14:   */   {
/* 15:   */     try
/* 16:   */     {
/* 17:21 */       JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { ABSA15Reviews.class });
/* 18:22 */       this.jaxbMarshaller = jaxbContext.createMarshaller();
/* 19:   */       
/* 20:   */ 
/* 21:25 */       this.jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
/* 22:   */       
/* 23:   */ 
/* 24:28 */       this.jaxbMarshaller.setProperty("jaxb.encoding", "UTF-8");
/* 25:   */       
/* 26:   */ 
/* 27:31 */       this.jaxbMarshaller.setProperty(
/* 28:32 */         CharacterEscapeHandler.class.getName(), 
/* 29:33 */         new CharacterEscapeHandler()
/* 30:   */         {
/* 31:   */           public void escape(char[] arg0, int arg1, int arg2, boolean arg3, Writer arg4)
/* 32:   */             throws IOException
/* 33:   */           {
/* 34:38 */             arg4.write(arg0, arg1, arg2);
/* 35:   */           }
/* 36:   */         });
/* 37:   */     }
/* 38:   */     catch (Exception e)
/* 39:   */     {
/* 40:44 */       e.printStackTrace();
/* 41:   */     }
/* 42:   */   }
/* 43:   */   
/* 44:   */   public Marshaller getJaxbMarshaller()
/* 45:   */   {
/* 46:49 */     return this.jaxbMarshaller;
/* 47:   */   }
/* 48:   */   
/* 49:   */   public void setJaxbMarshaller(Marshaller jaxbMarshaller)
/* 50:   */   {
/* 51:53 */     this.jaxbMarshaller = jaxbMarshaller;
/* 52:   */   }
/* 53:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.SemEval2015Marshaller
 * JD-Core Version:    0.7.0.1
 */