/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import java.util.ArrayList;
/*  4:   */ import javax.xml.bind.annotation.XmlElement;
/*  5:   */ import javax.xml.bind.annotation.XmlRootElement;
/*  6:   */ 
/*  7:   */ @XmlRootElement(name="Reviews")
/*  8:   */ public class ABSA15Reviews
/*  9:   */ {
/* 10:   */   private ArrayList<ABSA15Review> reviews;
/* 11:   */   
/* 12:   */   public ABSA15Reviews()
/* 13:   */   {
/* 14:15 */     this.reviews = new ArrayList();
/* 15:   */   }
/* 16:   */   
/* 17:   */   public ArrayList<ABSA15Review> getReviews()
/* 18:   */   {
/* 19:19 */     return this.reviews;
/* 20:   */   }
/* 21:   */   
/* 22:   */   @XmlElement(name="Review")
/* 23:   */   public void setReviews(ArrayList<ABSA15Review> reviews)
/* 24:   */   {
/* 25:25 */     this.reviews = reviews;
/* 26:   */   }
/* 27:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews
 * JD-Core Version:    0.7.0.1
 */