/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import java.io.Serializable;
/*  4:   */ import javax.xml.bind.annotation.XmlAttribute;
/*  5:   */ import javax.xml.bind.annotation.XmlElement;
/*  6:   */ import javax.xml.bind.annotation.XmlRootElement;
/*  7:   */ 
/*  8:   */ @XmlRootElement(name="Review")
/*  9:   */ public class ABSA15Review
/* 10:   */   implements Serializable
/* 11:   */ {
/* 12:   */   private ABSA15Sentences sentences;
/* 13:   */   private String rid;
/* 14:   */   
/* 15:   */   public ABSA15Review()
/* 16:   */   {
/* 17:17 */     this.sentences = new ABSA15Sentences();
/* 18:   */   }
/* 19:   */   
/* 20:   */   public ABSA15Sentences getSentences()
/* 21:   */   {
/* 22:21 */     return this.sentences;
/* 23:   */   }
/* 24:   */   
/* 25:   */   @XmlElement(name="sentences")
/* 26:   */   public void setSentences(ABSA15Sentences sentences)
/* 27:   */   {
/* 28:26 */     this.sentences = sentences;
/* 29:   */   }
/* 30:   */   
/* 31:   */   public String getRid()
/* 32:   */   {
/* 33:30 */     return this.rid;
/* 34:   */   }
/* 35:   */   
/* 36:   */   @XmlAttribute(name="rid")
/* 37:   */   public void setRid(String rid)
/* 38:   */   {
/* 39:35 */     this.rid = rid;
/* 40:   */   }
/* 41:   */   
/* 42:   */   public boolean equals(Object o)
/* 43:   */   {
/* 44:40 */     if ((o instanceof ABSA15Review)) {
/* 45:42 */       if (((ABSA15Review)o).getRid().equals(this.rid)) {
/* 46:44 */         return true;
/* 47:   */       }
/* 48:   */     }
/* 49:48 */     return false;
/* 50:   */   }
/* 51:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review
 * JD-Core Version:    0.7.0.1
 */