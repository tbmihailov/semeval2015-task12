/*  1:   */ package gr.ilsp.nlp.semeval.absa15.xml;
/*  2:   */ 
/*  3:   */ import java.io.Serializable;
/*  4:   */ import java.util.ArrayList;
/*  5:   */ import javax.xml.bind.annotation.XmlElement;
/*  6:   */ import javax.xml.bind.annotation.XmlRootElement;
/*  7:   */ 
/*  8:   */ @XmlRootElement(name="Opinions")
/*  9:   */ public class ABSA15OpiSet
/* 10:   */   implements Serializable
/* 11:   */ {
/* 12:   */   private ArrayList<ABSA15Opinion> opiList;
/* 13:   */   
/* 14:   */   public ABSA15OpiSet()
/* 15:   */   {
/* 16:14 */     this.opiList = new ArrayList();
/* 17:   */   }
/* 18:   */   
/* 19:   */   public ArrayList<ABSA15Opinion> getOpiList()
/* 20:   */   {
/* 21:21 */     return this.opiList;
/* 22:   */   }
/* 23:   */   
/* 24:   */   @XmlElement(name="Opinion")
/* 25:   */   public void setOpiList(ArrayList<ABSA15Opinion> opiList)
/* 26:   */   {
/* 27:27 */     this.opiList = opiList;
/* 28:   */   }
/* 29:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet
 * JD-Core Version:    0.7.0.1
 */