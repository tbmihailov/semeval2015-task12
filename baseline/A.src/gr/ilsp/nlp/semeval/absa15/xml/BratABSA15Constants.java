package gr.ilsp.nlp.semeval.absa15.xml;

public class BratABSA15Constants
{
  public static final String NOTANNO = "NotAnno";
  public static final String Target = "Target";
  public static final String POL = "Polarity";
  public static final String CAT = "CategoryType";
  public static final String CATA = "CategoryA";
  public static final String CATB = "CategoryB";
  public static final String NULLTARGET = "NULLTARGET";
  public static final String NotConfident = "NotConfident";
  public static final String OOS = "OutOfScope";
}


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.xml.BratABSA15Constants
 * JD-Core Version:    0.7.0.1
 */