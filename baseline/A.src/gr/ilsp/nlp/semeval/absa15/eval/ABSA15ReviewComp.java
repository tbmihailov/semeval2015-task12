/*  1:   */ package gr.ilsp.nlp.semeval.absa15.eval;
/*  2:   */ 
/*  3:   */ import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
/*  4:   */ import java.util.Comparator;
/*  5:   */ 
/*  6:   */ public class ABSA15ReviewComp
/*  7:   */   implements Comparator<ABSA15Review>
/*  8:   */ {
/*  9:   */   public int compare(ABSA15Review r1, ABSA15Review r2)
/* 10:   */   {
/* 11:13 */     int ret = r1.getRid().compareTo(r2.getRid());
/* 12:15 */     if (ret > 0) {
/* 13:17 */       return 1;
/* 14:   */     }
/* 15:19 */     if (ret < 0) {
/* 16:21 */       return -1;
/* 17:   */     }
/* 18:24 */     return 0;
/* 19:   */   }
/* 20:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.eval.ABSA15ReviewComp
 * JD-Core Version:    0.7.0.1
 */