package gr.ilsp.nlp.semeval.absa15.eval;

import gr.ilsp.nlp.formatutils.FormatUtils;
import gr.ilsp.nlp.semeval.absa15.baselines.PolarityLabelsMapping;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

public class ABSA15Eval
{
  PrintStream strm = System.out;
  private boolean sameSets = false;
  private int type;
  public static final int EvalCat = 1;
  public static final int EvalTrg = 2;
  public static final int EvalCatTrg = 3;
  public static final int EvalCatTrgPol = 4;
  public static final int EvalPol = 5;
  private FormatUtils FU;
  private int maxPadding = 17;
  private ABSA15Unmarshaller unm;
  private int[] overallCounts = null;
  private int[][] confusion;
  private ArrayList<Float> F1PerSentence = null;
  boolean removeDuplicates = false;
  
  public ABSA15Eval(boolean sameSets, int type)
  {
    this.unm = new ABSA15Unmarshaller();
    this.FU = new FormatUtils();
    
    this.sameSets = sameSets;
    if (sameSets) {
      this.confusion = new int[4][4];
    } else {
      this.overallCounts = new int[3];
    }
    this.type = type;
  }
  
  public boolean isRemoveDuplicates()
  {
    return this.removeDuplicates;
  }
  
  public void setRemoveDuplicates(boolean removeDuplicates)
  {
    this.removeDuplicates = removeDuplicates;
  }
  
  public HashMap<String, ABSA15Review> toMap(ABSA15Reviews revs)
  {
    HashMap<String, ABSA15Review> map = new HashMap();
    for (int i = 0; i < revs.getReviews().size(); i++)
    {
      ABSA15Review r = (ABSA15Review)revs.getReviews().get(i);
      map.put(r.getRid(), r);
    }
    return map;
  }
  
  public HashMap<String, ABSA15Sentence> toMap(ABSA15Review rev)
  {
    HashMap<String, ABSA15Sentence> map = new HashMap();
    ArrayList<ABSA15Sentence> sentences = rev.getSentences().getSentenceList();
    for (int i = 0; i < sentences.size(); i++) {
      map.put(((ABSA15Sentence)sentences.get(i)).getId(), (ABSA15Sentence)sentences.get(i));
    }
    return map;
  }
  
  public static float[] measures(int[] counts)
  {
    float PRE = counts[2] / counts[0];
    float REC = counts[2] / counts[1];
    float F1 = 2.0F * PRE * REC / (PRE + REC);
    
    float[] results = new float[3];
    results[0] = PRE;
    results[1] = REC;
    results[2] = F1;
    
    return results;
  }
  
  private void eval(ABSA15Reviews revsSys, ABSA15Reviews revsGld)
  {
    int revSysSize = revsSys.getReviews().size();
    int revGoldSize = revsGld.getReviews().size();
    
    HashMap<String, ABSA15Review> syst = toMap(revsSys);
    HashMap<String, ABSA15Review> gold = toMap(revsGld);
    
    Iterator<String> itSyst = syst.keySet().iterator();
    Iterator<String> itGold = gold.keySet().iterator();
    
    String rid = "";
    String sid = "";
    try
    {
      this.F1PerSentence = new ArrayList();
      Iterator<String> it;
      for (; itSyst.hasNext(); it.hasNext())
      {
        rid = (String)itSyst.next();
        
        HashMap<String, ABSA15Sentence> systSent = toMap((ABSA15Review)syst.get(rid));
        HashMap<String, ABSA15Sentence> goldSent = toMap((ABSA15Review)gold.get(rid));
        
        it = systSent.keySet().iterator();
        
        //continue;TODO:Check if this is working
        
        sid = (String)it.next();
        
        ABSA15Sentence s = (ABSA15Sentence)systSent.get(sid);
        ABSA15Sentence g = (ABSA15Sentence)goldSent.get(sid);
        if (s.getOOS() == null) {
          compareOpinions(s, g);
        }
      }
    }
    catch (Exception e)
    {
      this.strm.println("id with error:" + rid + " " + sid);
      e.printStackTrace();
    }
  }
  
  HashSet<Integer> m1 = null;
  HashSet<Integer> m2 = null;
  private int A = 0;
  private int B = 0;
  private int C = 0;
  
  private ArrayList<ABSA15Opinion> removeNULL(ArrayList<ABSA15Opinion> list, String id, String s)
  {
    HashSet<String> set = new HashSet();
    ArrayList<ABSA15Opinion> filt = new ArrayList();
    if (this.type == 2) {
      for (int i = 0; i < list.size(); i++)
      {
        String label = ((ABSA15Opinion)list.get(i)).getFrom() + "," + ((ABSA15Opinion)list.get(i)).getTo();
        if ((((ABSA15Opinion)list.get(i)).getTarget().equalsIgnoreCase("NULL")) || (((ABSA15Opinion)list.get(i)).getTarget().equalsIgnoreCase("")))
        {
          this.strm.println("Removing NULL ..." + label + " from " + id + " " + s);
        }
        else
        {
          filt.add((ABSA15Opinion)list.get(i));
          set.add(label);
        }
      }
    }
    if (this.type == 1) {
      for (int i = 0; i < list.size(); i++)
      {
        String label = ((ABSA15Opinion)list.get(i)).getCategory();
        if ((label.equalsIgnoreCase("NULL")) || (label.equalsIgnoreCase("")))
        {
          this.strm.println("Removing NULL ..." + label + " from " + id + " " + s);
        }
        else
        {
          filt.add((ABSA15Opinion)list.get(i));
          set.add(label);
        }
      }
    }
    return filt;
  }
  
  private ArrayList<ABSA15Opinion> removeDup(ArrayList<ABSA15Opinion> list, String id, String s)
  {
    HashSet<String> set = new HashSet();
    ArrayList<ABSA15Opinion> filt = new ArrayList();
    for (int i = 0; i < list.size(); i++)
    {
      String label = "";
      if (this.type == 2)
      {
        label = ((ABSA15Opinion)list.get(i)).getFrom() + "," + ((ABSA15Opinion)list.get(i)).getTo();
        if (!set.contains(label))
        {
          filt.add((ABSA15Opinion)list.get(i));
          set.add(label);
        }
        else
        {
          this.strm.println("removing duplicate ..." + label + " from " + id + " " + s);
        }
      }
      if (this.type == 1)
      {
        label = ((ABSA15Opinion)list.get(i)).getCategory();
        if (!set.contains(label))
        {
          filt.add((ABSA15Opinion)list.get(i));
          set.add(label);
        }
        else
        {
          this.strm.println("removing duplicate ..." + label + " from " + id + " " + s);
        }
      }
    }
    return filt;
  }
  
  private void compareOpinions(ABSA15Sentence sys, ABSA15Sentence gold)
  {
    this.m1 = new HashSet();
    this.m2 = new HashSet();
    
    this.A = 0;
    this.B = 0;
    this.C = 0;
    
    ArrayList<ABSA15Opinion> sysList = sys.getOp().getOpiList();
    ArrayList<ABSA15Opinion> gldList = gold.getOp().getOpiList();
    if (this.type == 2)
    {
      sysList = removeNULL(sysList, sys.getId(), "system");
      sysList = removeDup(sysList, gold.getId(), "system");
      
      gldList = removeNULL(gldList, sys.getId(), "gold");
      gldList = removeDup(gldList, gold.getId(), "gold");
    }
    if (this.type == 1)
    {
      sysList = removeNULL(sysList, sys.getId(), "system");
      sysList = removeDup(sysList, gold.getId(), "system");
      
      gldList = removeNULL(gldList, sys.getId(), "gold");
      gldList = removeDup(gldList, gold.getId(), "gold");
    }
    for (int i = 0; i < sysList.size(); i++)
    {
      ABSA15Opinion o1 = (ABSA15Opinion)sysList.get(i);
      for (int j = 0; j < gldList.size(); j++)
      {
        ABSA15Opinion o2 = (ABSA15Opinion)gldList.get(j);
        if ((!this.m1.contains(Integer.valueOf(i))) && (!this.m2.contains(Integer.valueOf(j)))) {
          if (!isSameSets()) {
            match1(o1, o2, i, j);
          } else if (i == j) {
            match2(o1, o2);
          }
        }
      }
    }
    if (!isSameSets())
    {
      this.A = sysList.size();
      this.B = gldList.size();
      
      this.overallCounts[0] += this.A;
      this.overallCounts[1] += this.B;
      
      float p = 1.0F;
      float r = 1.0F;
      if (this.A != 0) {
        p = this.C / this.A;
      }
      if (this.B != 0) {
        r = this.C / this.B;
      }
      float f = 0.0F;
      if (p + r != 0.0F) {
        f = 2.0F * p * r / (p + r);
      }
      this.F1PerSentence.add(Float.valueOf(f));
    }
  }
  
  public void evaluator(String syst, String gold)
  {
    try
    {
      this.strm.println("Comparing:" + syst + " to " + gold);
      

      ABSA15Reviews revsSys = this.unm.read(syst);
      ABSA15Reviews goldSys = this.unm.read(gold);
      




      eval(revsSys, goldSys);
      if (isSameSets())
      {
        if (this.type == 5) {
          this.strm.println("Polarity Evaluation\n");
        }
        Iterator<String> it = PolarityLabelsMapping.map.keySet().iterator();
        ArrayList<String> labels = new ArrayList();
        while (it.hasNext()) {
          labels.add((String)it.next());
        }
        this.strm.println(FormatUtils.appender("-", "", this.maxPadding * 4 + 4));
        this.strm.println(FormatUtils.appender(" ", "label\\measure", this.maxPadding) + "|" + 
          FormatUtils.appender(" ", "Precision", this.maxPadding) + "|" + 
          FormatUtils.appender(" ", "Recall", this.maxPadding) + "|" + 
          FormatUtils.appender(" ", "F-measure", this.maxPadding) + "|");
        this.strm.println(FormatUtils.appender("-", "", this.maxPadding * 4 + 4));
        
        int correct = 0;
        int all = 0;
        for (int i = 0; i < labels.size(); i++)
        {
          int sumRow = 0;
          for (int j = 0; j < this.confusion.length; j++)
          {
            sumRow += this.confusion[i][j];
            if (i == j) {
              correct += this.confusion[i][j];
            }
            all += this.confusion[i][j];
          }
          int sumCol = 0;
          for (int j = 0; j < this.confusion.length; j++) {
            sumCol += this.confusion[j][i];
          }
          float P = this.confusion[i][i] / sumRow;
          float R = this.confusion[i][i] / sumCol;
          float F = 2.0F * P * R / (P + R);
          
          this.strm.print(
          
            FormatUtils.appender(" ", (String)labels.get(i), this.maxPadding) + "|" + 
            FormatUtils.appender(" ", new StringBuilder(String.valueOf(this.FU.yield(P))).append("(").append(this.confusion[i][i]).append("/").append(sumRow).append(")").toString(), this.maxPadding) + "|" + 
            FormatUtils.appender(" ", new StringBuilder(String.valueOf(this.FU.yield(R))).append("(").append(this.confusion[i][i]).append("/").append(sumCol).append(")").toString(), this.maxPadding) + "|" + 
            FormatUtils.appender(" ", new StringBuilder(String.valueOf(this.FU.yield(F))).toString(), this.maxPadding) + "|");
          

          this.strm.print("\n");
        }
        this.strm.println(FormatUtils.appender("-", "", this.maxPadding * 4 + 4));
        
        float accuracy = correct / all;
        this.strm.println("Accuracy:\t" + accuracy + " (" + correct + "/" + all + ")");
      }
      else
      {
        if (this.type == 1) {
          this.strm.println("\n** Categories Evaluation **\n");
        }
        if (this.type == 2) {
          this.strm.println("\n** Targets Evaluation ** \n");
        }
        if (this.type == 3) {
          this.strm.println("\n** Evaluation for (category, target) tuples ** \n");
        }
        this.strm.println("#predicted=" + this.overallCounts[0] + "\n#gold=" + this.overallCounts[1] + "\n#common=" + this.overallCounts[2]);
        float[] meas = measures(this.overallCounts);
        this.strm.println("PRE=" + meas[0] + "\nREC=" + meas[1] + "\nF-MEASURE=" + meas[2]);
        
        float sum = 0.0F;
        for (int i = 0; i < this.F1PerSentence.size(); i++) {
          sum += ((Float)this.F1PerSentence.get(i)).floatValue();
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  private void match1(ABSA15Opinion o1, ABSA15Opinion o2, int i, int j)
  {
    boolean slot1 = o1.getCategory().equals(o2.getCategory());
    boolean slot2 = (o1.getFrom() == o2.getFrom()) && (o1.getTo() == o2.getTo());
    boolean slot3 = o1.getPolarity().equals(o2.getPolarity());
    if ((this.type == 4) && (slot1) && (slot2) && (slot3))
    {
      this.overallCounts[2] += 1;
      this.C += 1;
      this.m1.add(Integer.valueOf(i));
      this.m2.add(Integer.valueOf(j));
    }
    if ((this.type == 3) && (slot1) && (slot2))
    {
      this.overallCounts[2] += 1;
      this.C += 1;
      this.m1.add(Integer.valueOf(i));
      this.m2.add(Integer.valueOf(j));
    }
    if ((this.type == 1) && (slot1))
    {
      this.overallCounts[2] += 1;
      this.C += 1;
      this.m1.add(Integer.valueOf(i));
      this.m2.add(Integer.valueOf(j));
    }
    if ((this.type == 2) && (slot2))
    {
      this.overallCounts[2] += 1;
      this.C += 1;
      this.m1.add(Integer.valueOf(i));
      this.m2.add(Integer.valueOf(j));
    }
  }
  
  private void match2(ABSA15Opinion o1, ABSA15Opinion o2)
  {
    boolean slot1 = o1.getCategory().equals(o2.getCategory());
    boolean slot2 = (o1.getFrom() == o2.getFrom()) && (o1.getTo() == o2.getTo());
    
    int a = ((Integer)PolarityLabelsMapping.map.get(o1.getPolarity())).intValue();
    int b = ((Integer)PolarityLabelsMapping.map.get(o2.getPolarity())).intValue();
    this.confusion[a][b] += 1;
    if ((!slot1) || (!slot2)) {
      this.strm.println("ISSUE slot1 and slot 2 do not match:" + o1.getCategory() + " " + o2.getCategory());
    }
  }
  
  public boolean isSameSets()
  {
    return this.sameSets;
  }
  
  public void setSameSets(boolean sameSets)
  {
    this.sameSets = sameSets;
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public void setType(int type)
  {
    this.type = type;
  }
  
  public static void main(String[] args)
  {
    String syst = args[0];
    String gold = args[1];
    int type = Integer.parseInt(args[2]);
    int s = Integer.parseInt(args[3]);
    
    boolean same = false;
    if (s == 1) {
      same = true;
    } else if (s == 0) {
      same = false;
    }
    System.err.println();
    ABSA15Eval eval = new ABSA15Eval(same, type);
    eval.evaluator(syst, gold);
    System.err.println();
  }
}
