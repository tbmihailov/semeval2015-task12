/*  1:   */ package gr.ilsp.nlp.semeval.absa15.fileutils;
/*  2:   */ 
/*  3:   */ import java.io.File;
/*  4:   */ import java.io.FilenameFilter;
/*  5:   */ 
/*  6:   */ public class GetFiles
/*  7:   */ {
/*  8:   */   public static File[] getFilesWithSuffix(File inputDir, String suff)
/*  9:   */   {
/* 10:11 */     File[] FilesArray = inputDir.listFiles(
/* 11:   */     
/* 12:13 */       new FilenameFilter()
/* 13:   */       {
/* 14:   */         public boolean accept(File dir, String name)
/* 15:   */         {
/* 16:17 */           return name.toLowerCase().endsWith(suff);
/* 17:   */         }
/* 18:21 */       });
/* 19:22 */     return FilesArray;
/* 20:   */   }
/* 21:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa15.fileutils.GetFiles
 * JD-Core Version:    0.7.0.1
 */