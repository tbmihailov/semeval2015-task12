/*  1:   */ package gr.ilsp.nlp.semeval.absa.eval;
/*  2:   */ 
/*  3:   */ import java.io.FileInputStream;
/*  4:   */ import java.io.InputStream;
/*  5:   */ import java.io.PrintStream;
/*  6:   */ import javax.xml.transform.stream.StreamSource;
/*  7:   */ import javax.xml.validation.Schema;
/*  8:   */ import javax.xml.validation.SchemaFactory;
/*  9:   */ import javax.xml.validation.Validator;
/* 10:   */ 
/* 11:   */ public class ConsoleXmlValidator
/* 12:   */ {
/* 13:   */   public static void main(String[] args)
/* 14:   */   {
/* 15:   */     try
/* 16:   */     {
/* 17:18 */       String xmlFile = "";
/* 18:19 */       String xsdFile = "";
/* 19:   */       
/* 20:21 */       xmlFile = args[0];
/* 21:22 */       xsdFile = args[1];
/* 22:   */       
/* 23:24 */       FileInputStream fileS = new FileInputStream(xmlFile);
/* 24:25 */       FileInputStream xsdS = new FileInputStream(xsdFile);
/* 25:   */       
/* 26:27 */       System.out.println("Validating \n" + xmlFile + " against xsd\n" + xsdFile);
/* 27:28 */       boolean ret = validateAgainstXSD(fileS, xsdS);
/* 28:30 */       if (ret) {
/* 29:32 */         System.out.println("Given xml is valid!");
/* 30:   */       } else {
/* 31:36 */         System.out.println("Given xml is not valid!");
/* 32:   */       }
/* 33:   */     }
/* 34:   */     catch (Exception e)
/* 35:   */     {
/* 36:42 */       e.printStackTrace();
/* 37:   */     }
/* 38:   */   }
/* 39:   */   
/* 40:   */   static boolean validateAgainstXSD(InputStream xml, InputStream xsd)
/* 41:   */   {
/* 42:   */     try
/* 43:   */     {
/* 44:52 */       SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
/* 45:53 */       Schema schema = factory.newSchema(new StreamSource(xsd));
/* 46:54 */       Validator validator = schema.newValidator();
/* 47:55 */       validator.validate(new StreamSource(xml));
/* 48:56 */       return true;
/* 49:   */     }
/* 50:   */     catch (Exception ex)
/* 51:   */     {
/* 52:60 */       ex.printStackTrace();
/* 53:   */     }
/* 54:61 */     return false;
/* 55:   */   }
/* 56:   */ }


/* Location:           D:\Programming\TextMining\SemEval2015\baseline\baseevalvalid1\BaseEvalValid1\A.jar
 * Qualified Name:     gr.ilsp.nlp.semeval.absa.eval.ConsoleXmlValidator
 * JD-Core Version:    0.7.0.1
 */