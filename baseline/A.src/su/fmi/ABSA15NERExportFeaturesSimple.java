package su.fmi;

import gr.ilsp.nlp.semeval.absa15.baselines.TokenFreq;
import gr.ilsp.nlp.semeval.absa15.baselines.Utils;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

public class ABSA15NERExportFeaturesSimple {

	public void extractTermFeaturesSingle(int mode, String inputReviewsFile,
			String outputTrain, String outputTest) {
		extractTermFeaturesSingle(mode, inputReviewsFile, outputTrain,
				outputTest, 0.9);
	}

	public void extractTermFeaturesSingle(int mode, String inputReviewsFile,
			String outputTrain, String outputTest, Double trainCoef) {
		try {
			ArrayList<TokenFreq> array = new ArrayList();
			String line = "";

			System.err.println("Create and Write NER Mallet simple input...");

			ArrayList<ABSA15Review> reviewsAll = ReadReviewsFromFile(inputReviewsFile);

			ArrayList<ABSA15Review> reviewsTrain = new ArrayList<ABSA15Review>();
			ArrayList<ABSA15Review> reviewsTest = new ArrayList<ABSA15Review>();
			splitToTrainTest(reviewsAll, reviewsTrain, reviewsTest, trainCoef);

			extractTokenPerLineWithClassToFile(mode, reviewsTrain, outputTrain);
			extractTokenPerLineWithClassToFile(mode, reviewsTest, outputTest);

			System.err.println("Finished!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ArrayList<ABSA15Review> ReadReviewsFromFile(String inputReviewsFile)
			throws Exception {
		ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
		ABSA15Reviews revs = null;

		revs = unm.read(inputReviewsFile);
		ArrayList<ABSA15Review> reviewsAll = revs.getReviews();
		return reviewsAll;
	}

	private void splitToTrainTest(ArrayList<ABSA15Review> reviewsAll,
			ArrayList<ABSA15Review> reviewsTrain,
			ArrayList<ABSA15Review> reviewsTest, Double trainCoef) {

		int reviewsAllCnt = reviewsAll.size();
		int reviewsTrainCnt = (int) Math.round(reviewsAllCnt * trainCoef);
		int reviewsTestCnt = reviewsAllCnt - reviewsTrainCnt;

		// fill train
		for (int i = 0; i < reviewsAll.size(); i++) {
			reviewsTrain.add(reviewsAll.get(i));
		}

		// fill test
		Random random = new Random();
		while (reviewsTest.size() < reviewsTestCnt) {
			int randomIndex = random.nextInt(reviewsTrain.size());
			ABSA15Review review = reviewsTrain.get(randomIndex);
			reviewsTest.add(review);
			reviewsTrain.remove(review);
		}

	}

	private void extractTokenPerLineWithClassToFile(int mode,
			String inputReviewsFile, String outputFile) throws Exception {
		ArrayList<ABSA15Review> reviews = ReadReviewsFromFile(inputReviewsFile);
		extractTokenPerLineWithClassToFile(mode, reviews, outputFile);
	}

	private void extractTokenPerLineWithClassToFile(int mode,
			ArrayList<ABSA15Review> reviews, String outputFile)
			throws FileNotFoundException, IOException {
		FileOutputStream fileTrain = new FileOutputStream(outputFile);
		for (int i = 0; i < reviews.size(); i++) {
			ABSA15Sentences sentences = ((ABSA15Review) reviews.get(i))
					.getSentences();
			for (int j = 0; j < sentences.getSentenceList().size(); j++) {
				ABSA15Sentence sent = (ABSA15Sentence) sentences
						.getSentenceList().get(j);
				ABSA15OpiSet opinions = sent.getOp();

				if (sent.getOOS() == null) {
					String text = sent.getText();

					List<String> tokenExcludeFilter = Arrays
							.asList(new String[] { " " });
					su.fmi.util.StringTokenizer st = new su.fmi.util.StringTokenizer(
							text, " ,.!'()-/\\", true);
					while (st.hasMoreTokens()) {
						String token = st.nextToken();
						if (tokenExcludeFilter.contains(token)) {
							continue;
						}
						int to = st.getCurrentPosition();
						int from = st.getCurrentPosition() - token.length();

						ABSA15Opinion targetOpinion = getOpinionIfTarget(token,
								from, to, opinions);
						String classInfo = "O";
						if (targetOpinion != null) {
							if (mode == 1) {
								boolean isTarget = isTarget(token, from, to,
										opinions);
								classInfo = isTarget ? "T" : "O";
							} else if (mode == 2) {
								classInfo = targetOpinion.getCategory();
							} else if (mode == 3) {
								classInfo = targetOpinion.getCategory().split(
										"#")[0];
							}

						}
						String str = String
								.format("%s\t%s\n", token, classInfo);
						fileTrain.write(str.getBytes());
						fileTrain.flush();

					}
				}
			}
		}

		fileTrain.close();
	}

	private ABSA15Opinion getOpinionIfTarget(String token, int from, int to,
			ABSA15OpiSet opinions) {
		ArrayList<ABSA15Opinion> opiList = opinions.getOpiList();
		for (int i = 0; i < opiList.size(); i++) {
			ABSA15Opinion opi = opiList.get(i);
			if (opi.getFrom() == from && opi.getTo() == to) {
				return opi;
			}
		}

		return null;
	}

	private boolean isTarget(String token, int from, int to,
			ABSA15OpiSet opinions) {
		ArrayList<ABSA15Opinion> opiList = opinions.getOpiList();
		for (int i = 0; i < opiList.size(); i++) {
			ABSA15Opinion opi = opiList.get(i);
			if (opi.getFrom() == from && opi.getTo() == to) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		String action = args[0];
		if (action.equals("single_file")) {
			System.out.println("=====Extract features for NER sample from PRESLAV=====");
			int mode = 1;
			try {
				Integer.parseInt(args[1]);
			} catch (Exception e) {
				System.out.print(e.toString());
			}

			String inputFile = args[2];
			String outputFile = args[3];

			ABSA15NERExportFeaturesSimple extractor = new ABSA15NERExportFeaturesSimple();
			extractor.extractTokenPerLineWithClassToFile(mode, inputFile,
					outputFile);

			System.out.println("Output:"+outputFile);
			System.out.println("Finish!");
		} else if (action.equals("test")) {
			int mode = 1;
			try {
				Integer.parseInt(args[1]);
			} catch (Exception e) {
				System.out.print(e.toString());
			}

			String inputFile = args[2];
			String outputFileTrain = args[3];
			String outputFileTest = args[4];
			double trainCoef = 0.9;

			ABSA15NERExportFeaturesSimple extractor = new ABSA15NERExportFeaturesSimple();
			extractor.extractTermFeaturesSingle(mode, inputFile,
					outputFileTrain, outputFileTest, trainCoef);
		} else {
			throw new Exception("Invalid input \"" + action + "\"");
		}
	}
}
