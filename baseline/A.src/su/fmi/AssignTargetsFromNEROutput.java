package su.fmi;

import gr.ilsp.nlp.semeval.absa15.baselines.DetectTargets;
import gr.ilsp.nlp.semeval.absa15.baselines.Settings;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15OpiSet;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Opinion;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Review;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Reviews;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentence;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Sentences;
import gr.ilsp.nlp.semeval.absa15.xml.ABSA15Unmarshaller;
import gr.ilsp.nlp.semeval.absa15.xml.SemEval2015Marshaller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.Marshaller;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class AssignTargetsFromNEROutput {

	private Settings settings;

	public AssignTargetsFromNEROutput(Settings settings) {
		this.settings = settings;
	}

	public void assign(int mode, String outputFile, String inputFile) {
		try {
			System.err.println("Assigning targets");
			String punctutations = ".,:;!";
			String test = this.settings.getTeCln_PrdAspTrg();

			InputStream fisInputFile = new FileInputStream(inputFile);
			BufferedReader brInputFile = new BufferedReader(
					(new InputStreamReader(fisInputFile,
							Charset.forName("UTF-8"))));
			String line = "";

			ABSA15Unmarshaller unm = new ABSA15Unmarshaller();
			ABSA15Reviews revs = unm.read(test);

			int lineIndex = 0;
			for (int i = 0; i < revs.getReviews().size(); i++) {
				ABSA15Sentences sentences = ((ABSA15Review) revs.getReviews()
						.get(i)).getSentences();
				for (int j = 0; j < sentences.getSentenceList().size(); j++) {
					ABSA15Sentence sent = (ABSA15Sentence) sentences
							.getSentenceList().get(j);
					String text = sent.getText();

					ABSA15OpiSet opinions = sent.getOp();
					if (sent.getOOS() == null) {
						List<String> tokenExcludeFilter = Arrays
								.asList(new String[] { " " });
						su.fmi.util.StringTokenizer st = new su.fmi.util.StringTokenizer(
								text, " ,.!'()-/\\", true);
						while (st.hasMoreTokens()) {
							String token = st.nextToken();
							if (tokenExcludeFilter.contains(token)) {
								continue;
							}
							int to = st.getCurrentPosition();
							int from = st.getCurrentPosition() - token.length();

							// infput file with annotated terms from MALLET CRF
							// sample
							line = brInputFile.readLine();
							lineIndex++;

							Matcher matcher = Pattern.compile(
									"^(\\S+)\\s.*CUR=(\\S+)\\s").matcher(line);
							if (!matcher.find()) {
								throw new Exception("No mathes!");
							}
							String classInfo = matcher.group(1);
							String term = matcher.group(2);

							// Check if term from the annotated file and token
							// from the review match
							if (token.equalsIgnoreCase(term)) {
								//System.out.println(String.format(
								//		"[%d] %s - %s - match", lineIndex,
								//		token, term));
							} else {
								throw new Exception(String.format(
										"[%d] %s - %s - NO match", lineIndex,
										token, term));
							}

							// check the NER mode - how is class represented in
							// the NER mallet sample
							if (mode == 1) {
								//classInfo is T - term or O - non term
								if (!classInfo.equals("O")) {
									if (opinions.getOpiList().size() > 0) {
										boolean isCurrentlyTarget = false;
										for (ABSA15Opinion opi : opinions
												.getOpiList()) {
											if (!opi.getTarget().equals("NULL")
													&& (opi.getFrom() == from)
													&& (opi.getTo() == to)) {
												isCurrentlyTarget = true;
											}
										}

										// check if some smarter target
										// recognition hasn't already found the
										// target
										if (!isCurrentlyTarget) {
											// set to the first opi - we don't
											// know how to match category anyway
											ABSA15Opinion opi = getNonTermOpinionIfAny(opinions
													.getOpiList());
											if (opi == null) {
												opi = new ABSA15Opinion();
												opinions.getOpiList().add(opi);
											}
											opi.setTarget(token);
											opi.setFrom(from);
											opi.setTo(to);
										}
									} else {
										ABSA15Opinion opi = new ABSA15Opinion();
										opinions.getOpiList().add(opi);
										opi.setTarget(token);
										opi.setFrom(from);
										opi.setTo(to);
									}
								}
							} else if (mode == 2) {
								//mode=2 - classInfo is in format "category#aspect" or "O"
								if(!classInfo.equals("O")){
									if (opinions.getOpiList().size() > 0) {
										boolean isCurrentlyTarget = false;
										for (ABSA15Opinion opi : opinions
												.getOpiList()) {
											if (!opi.getTarget().equals("NULL")
													&& (opi.getFrom() == from)
													&& (opi.getTo() == to)) {
												isCurrentlyTarget = true;
											}
										}

										// check if some smarter target
										// recognition hasn't already found the
										// target
										if (!isCurrentlyTarget) {
											// set to the first opi - we don't
											// know how to match category anyway
											ABSA15Opinion opi = getNonTermOpinionIfAny(opinions
													.getOpiList());
											if (opi == null) {
												opi = new ABSA15Opinion();
												opinions.getOpiList().add(opi);
											}
											opi.setTarget(token);
											opi.setFrom(from);
											opi.setTo(to);
											
											//override previously selected category
											opi.setCategory(classInfo);
										}
									} else {
										ABSA15Opinion opi = new ABSA15Opinion();
										opinions.getOpiList().add(opi);
										opi.setTarget(token);
										opi.setFrom(from);
										opi.setTo(to);
										opi.setCategory(classInfo);
									}									
								}
							} else if (mode == 3) {
								throw new NotImplementedException();
								//classInfo = targetOpinion.getCategory().split(
								//		"#")[0];
							}

							// String cat = o.getCategory();
							// String target = "";
							// if (!target.equals("")) {
							// o.setFrom(from);
							// o.setTo(to);
							// o.setTarget(target);
							// } else {
							// o.setTarget("NULL");
							// }

						}
					}

				}
			}
			SemEval2015Marshaller marhshaller = new SemEval2015Marshaller();
			Marshaller jaxbMarshaller = marhshaller.getJaxbMarshaller();
			File output = new File(test);
			jaxbMarshaller.marshal(revs, output);
			System.err.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ABSA15Opinion getNonTermOpinionIfAny(
			ArrayList<ABSA15Opinion> opiList) {
		// TODO Auto-generated method stub
		return null;
	}

	private boolean isTarget(String token, int from, int to,
			ArrayList<ABSA15Opinion> opiList) {
		// TODO Auto-generated method stub
		return false;
	}

	public static void main(String[] args) {
		try {
			int mode = 1;
			try {
				Integer.parseInt(args[0]);
			} catch (Exception e) {
				System.out.print(e.toString());
			}

			String inputFile = args[1];
			String domain = args[2];
			String rootDir = args[3];
			String TrainTestDir = args[4];

			Settings settings = null;
			int partIdx = 0;
			if (args.length == 6) {
				partIdx = Integer.parseInt(args[5]);
				settings = new Settings(rootDir, TrainTestDir, domain, "", "."
						+ partIdx);
			} else {
				settings = new Settings(rootDir, TrainTestDir, domain, "", "");
			}

			AssignTargetsFromNEROutput trgs = new AssignTargetsFromNEROutput(
					settings);
			trgs.assign(mode, settings.getTeCln_PrdAspTrg(), inputFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
