package su.fmi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ABSA15NERResultEvaluator {

	public static void main(String[] args) {
		String testFile = args[0];
		String evaluatedFile = args[1];

		ABSA15NERResultEvaluator resCalculator = new ABSA15NERResultEvaluator();
		try {
			resCalculator.printPrecissionRecallF1(testFile, evaluatedFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Finished!");
	}

	public void printPrecissionRecallF1(String testFile, String evaluatedFile)
			throws IOException {

		// train file
		InputStream fisTrain = new FileInputStream(testFile);
		BufferedReader brTrain = new BufferedReader((new InputStreamReader(
				fisTrain, Charset.forName("UTF-8"))));
		String line = "";

		// scored file
		InputStream fisScored = new FileInputStream(evaluatedFile);
		BufferedReader brScored = new BufferedReader(new InputStreamReader(
				fisScored, Charset.forName("UTF-8")));
		String lineScore = "";

		// read parrallel from the two files and write to output
		try {
			int testAllScored = 0;
			int scoredRelevant = 0;
			int scoredAll = 0;

			int lineIndex = 0;
			while (((line = brTrain.readLine()) != null)
					&& ((lineScore = brScored.readLine()) != null)) {
				lineIndex++;
				String classTest = line.substring(line.lastIndexOf("\t") + 1);
				String classScored = lineScore.substring(0,
						lineScore.indexOf(" "));

				if (!classTest.equals("O")) {
					testAllScored++;
				}

				if (!classScored.equals("O")) {
					scoredAll++;
				}

				if ((!classScored.equals("O")) 
					&& (classTest.equals(classScored))) {
					scoredRelevant++;
				}
				else if (!classTest.equals(classScored)) {
					System.out.println(String.format("[%d - Wrong] scored %s but %s",lineIndex, classScored, classTest));
				}
			}

			double precission = (double)scoredRelevant / (double)testAllScored;
			double recall = (double)scoredRelevant / (double)scoredAll;
			double f1 = 2 * (precission * recall) / (precission + recall);

			System.out.println("Results");
			System.out.println(String.format("Scored all: %d", scoredAll));
			System.out.println(String.format("Scored pos: %d", scoredRelevant));
			System.out.println(String.format("Scored neg: %d", scoredAll
					- scoredRelevant));
			System.out.println(String.format("Golden all scored: %d", testAllScored));
			System.out.println("-----------------------");
			System.out.println(String.format("PRE: %f", precission));
			System.out.println(String.format("REC: %f", recall));
			System.out.println(String.format("F-1: %f", f1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Done with the files
		brTrain.close();
		brTrain = null;
		fisTrain = null;

		brScored.close();
		brScored = null;
		fisScored = null;
	}

	public int getScoreToTestComparison(String line, String lineScore) {
		String classTest = line.substring(line.lastIndexOf("\t") + 1);
		String classScored = lineScore.substring(0, lineScore.indexOf(" "));

		if (classTest == classScored) {
			return 1;
		}

		return 0;
	}

}
