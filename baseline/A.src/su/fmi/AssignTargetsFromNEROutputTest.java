package su.fmi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AssignTargetsFromNEROutputTest {

	public static void main(String[] args) throws Exception {
		String line = "T IS_PREV_STOPWORD PREV=the PREV_LOWER=the PREFIX2=de SUFFIX2=or CUR=decor CUR_LOWER=decor PREFIX4=deco PREFIX3=dec SUFFIX4=ecor SUFFIX3=cor PREV_LOWER=the&CUR_LOWER=decor ";

		Matcher matcher = Pattern.compile(
				"^(\\S+)\\s.*CUR=(\\S+)\\s").matcher(line);
		if (!matcher.find()) {
			throw new Exception("No mathes!");
		}
		String classInfo = matcher.group(1);
		String term = matcher.group(2);	
		System.out.println(String.format("%s %s", classInfo, term));
	}

}
