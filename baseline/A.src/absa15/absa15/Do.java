package absa15;

import gr.ilsp.nlp.semeval.absa15.baselines.Assign;
import gr.ilsp.nlp.semeval.absa15.baselines.CountAndCheck;
import gr.ilsp.nlp.semeval.absa15.baselines.CreateVecs;
import gr.ilsp.nlp.semeval.absa15.baselines.DetectTargets;
import gr.ilsp.nlp.semeval.absa15.baselines.ExtractFeats;
import gr.ilsp.nlp.semeval.absa15.baselines.Split;
import gr.ilsp.nlp.semeval.absa15.eval.ABSA15Eval;
import java.io.PrintStream;

public class Do
{
  public static void main(String[] args)
  {
    String cmd = args[0];
    String[] margs = new String[args.length - 1];
    System.arraycopy(args, 1, margs, 0, margs.length);
    if (cmd.equals("Validate")) {
      CountAndCheck.main(margs);
    } else if (cmd.equals("Split")) {
      Split.main(margs);
    } else if (cmd.equals("ExtractFeats")) {
      ExtractFeats.main(margs);
    } else if (cmd.equals("CreateVecs")) {
      CreateVecs.main(margs);
    } else if (cmd.equals("Assign")) {
      Assign.main(margs);
    } else if (cmd.equals("IdentifyTargets")) {
      DetectTargets.main(margs);
    } else if (cmd.equals("Eval")) {
      ABSA15Eval.main(margs);
    } else {
      System.err.println("COMMAND NOT FOUND" + cmd);
    }
  }
}
