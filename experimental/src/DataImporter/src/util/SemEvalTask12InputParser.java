package util;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SemEvalTask12InputParser {

	public static void main(String[] args) {
		String laptopReviewsInputPath = args[0];
		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

			File file = new File(laptopReviewsInputPath);
			Document dom = docBuilder.parse(file);

			// get the root element
			Element docEle = dom.getDocumentElement();

			// get a nodelist of <employee> elements
			NodeList nl = docEle.getElementsByTagName("Employee");
			if (nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {

					// get the employee element
					Element el = (Element) nl.item(i);

					printEmpl(el);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void printEmpl(Element el) {
		// TODO Auto-generated method stub

	}

}
