package dataimporter;

import java.io.File;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SemEvalTask12InputParser {

	public static void main(String[] args) {
		String laptopReviewsInputPath = args[0];
		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

			File file = new File(laptopReviewsInputPath);
			Document dom = docBuilder.parse(file);

			// get the root element
			Element docEle = dom.getDocumentElement();
			Element reviewElem = (Element) docEle.getElementsByTagName(
					"Reviews").item(0);
			// get a nodelist of <employee> elements
			NodeList nl = reviewElem.getElementsByTagName("Review");
			if (nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {
					// get the employee element
					Element review = (Element) nl.item(i);
					String reviewId = review.getAttribute("id");
					Element sentencesTag = (Element) review
							.getElementsByTagName("sentences").item(0);
					NodeList sentencesList = sentencesTag
							.getElementsByTagName("sentence");

					for (int j = 0; j < sentencesList.getLength(); j++) {
						Map<String, String> sentenceFeatures = new HashMap<String, String>();

						Element sentence = (Element) sentencesList.item(j);
						String sentenceIdStr = sentence.getAttribute("id");
						String sentenceReviewSequenceNumber = sentenceIdStr
								.split(":")[1];// id="12:2" - Review id 12 and
												// sequence number 2
						int isOutOfScope = 0;
						if (sentence.hasAttribute("OutOfScope")) {
							isOutOfScope = 1;
						}

						String text = ((Element) sentence.getElementsByTagName(
								"text").item(0)).getNodeValue();

						NodeList opinions = null;
						if (sentence.hasAttribute("Opinions")) {
							opinions = ((Element) sentence
									.getElementsByTagName("Opinions").item(0))
									.getElementsByTagName("opinion");
							int opinionsCount = opinions.getLength();
							for (int k = 0; k < opinions.getLength(); k++) {
								Element opinion = (Element) opinions.item(0);
								String opinionCategory = opinion
										.getAttribute("category");

								Attr polarityAttr = opinion
										.getAttributeNode("polarity");

								String opinionPolarityStr = polarityAttr
										.getValue();

								int opinionPolarity = 0;
								switch (opinionPolarityStr) {
								case "positive":
									opinionPolarity = 1;
									break;
								case "negative":
									opinionPolarity = -1;
									break;
								default:
									opinionPolarity = 0;
									break;
								}
							}
						}

					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void printEmpl(Element el) {
		// TODO Auto-generated method stub
	}

}
