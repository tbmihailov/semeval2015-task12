#!/usr/bin/perl -w
#
#
#  Author: Preslav Nakov
#          nakov@lml.bas.bg
#          BAS
#  
#  Description: Extracts features for training a CRF.
#
#  Last modified: May 6, 2008
#
#

use strict;


####################
###   STOPLIST   ###
####################

use constant STOPLIST => ('a','able','about','across','after','all','almost','also','am','among','an','and','any','are','as','at','be','because','been','but','by','can','cannot','could','dear','did','do','does','either','else','ever','every','for','from','get','got','had','has','have','he','her','hers','him','his','how','however','i','if','in','into','is','it','its','just','least','let','like','likely','may','me','might','most','must','my','neither','no','nor','not','of','off','often','on','only','or','other','our','own','rather','said','say','says','she','should','since','so','some','than','that','the','their','them','then','there','these','they','this','tis','to','too','twas','us','wants','was','we','were','what','when','where','which','while','who','whom','why','will','with','would','yet','you','your');


#####################
###   I/O FILES   ###
#####################

### Input file
use constant INPUT_FILE_TRAIN          => 'rest-train.txt';
use constant INPUT_FILE_TEST           => 'rest-test.txt';

### Output file
use constant OUTPUT_FILE_TRAIN         => 'rest-train-feat.txt';
use constant OUTPUT_FILE_TEST          => 'rest-test-feat.txt+';
use constant OUTPUT_FILE_TEST_NOLABEL  => 'rest-test-feat-noLabel.txt';


###################
###   GLOBALS   ###
###################

my %stopList = ();


########################
###                  ###
###   MAIN PROGRAM   ###
###                  ###
########################

### 1. Build a Bulgarian stoplist in a hash
foreach my $stopWord (STOPLIST) {
	$stopList{$stopWord}++;
}

### 2. Extract the features from the files
&extractFeatures(INPUT_FILE_TRAIN, OUTPUT_FILE_TRAIN, ());
&extractFeatures(INPUT_FILE_TEST,  OUTPUT_FILE_TEST,  OUTPUT_FILE_TEST_NOLABEL);


#######################
###                 ###
###   SUBROUTINES   ###
###                 ###
#######################

sub extractFeatures {
	my ($input, $output, $outputNoLabel) = @_;

	### 1. Open the files
	open(INPUT, $input) or die "Failed to open $input for reading!";
	open(OUTPUT, ">$output") or die "Failed to open $output for writing!";
	open(OUTPUT_NOLABEL, ">$outputNoLabel") or die "Failed to open $outputNoLabel for writing!" if (defined $outputNoLabel);

	### 2. Extract the features
	my $prevWord      = 'null';
	my $prevWordLower = 'null';
	while (<INPUT>) {

		### 2.1. Check for empty line
		if (/^[\n\r]*$/) {
			print OUTPUT "\n";
			print OUTPUT_NOLABEL "\n" if (defined $outputNoLabel);
			$prevWord      = 'null';
			$prevWordLower = 'null';
			next;
		}

		### 2.2. Check the file format and extract the word-tag pair
		#die "Wrong file format!" if (!/^([^ ]+)\t(T|O)[\n\r]*$/);
		die "Wrong file format!" if (!/^([^ ]+)\t(O|RESTAURANT|FOOD|DRINKS|SERVICE|AMBIENCE|LOCATION)[\n\r]*$/);
		my ($word, $tag, $wordLower) = ($1, $2, $1);
		$wordLower =~ tr/A-Z�-�/a-za-z/;

		### 2.4. Extract the features
		my $features = "CUR=$word";                                ## The current word itself
		$features .= " CUR_LOWER=$wordLower";                      ## The current word converted to lowercase
		$features .= " PREV=$prevWord";                            ## The previous word
		$features .= " PREV_LOWER=$prevWordLower";                 ## The previous word converted to lowercase
		$features .= ' NO_LETTERS' if ($word !~ /[a-zA-Z]/);       ## No letters
		$features .= ' ALL_CAPS' if ($word =~ /^[A-Z]+$/);         ## All caps
		$features .= ' INIT_CAP' if ($word =~ /^[A-Z][a-z]+$/);    ## Initial cap
		$features .= ' NUM' if ($word =~ /^[0-9]+$/);              ## Number
		$features .= ' ENDS_IN_DOT' if ($word =~ /\.$/);           ## Ends in Dot
		$features .= ' IS_STOPWORD' if (defined $stopList{$word}); ## Is current a stopword
		$features .= ' IS_PREV_STOPWORD' if (defined $stopList{$prevWord});## Is previous a stopword
		$features .= ' LEN<2' if (length $word < 2);               ## Length < 2
		$features .= ' LEN<3' if (length $word < 3);               ## Length < 3
		$features .= ' LEN>7' if (length $word > 7);               ## Length > 7
		$features .= ' PREV_LEN<3' if (length $prevWord < 3);      ## Previous word's length < 3
		$features .= " PREFIX4=" . substr($word,0,4);              ## Prefix of length 3
		$features .= " PREFIX3=" . substr($word,0,3);              ## Prefix of length 3
		$features .= " PREFIX2=" . substr($word,0,2);              ## Prefix of length 2
		$features .= " SUFFIX4=" . substr($word,-4);               ## Suffix of length 3
		$features .= " SUFFIX3=" . substr($word,-3);               ## Suffix of length 3
		$features .= " SUFFIX2=" . substr($word,-2);               ## Suffix of length 2
		$features .= ' INIT_CAP&PREV=punc' if ($word =~ /^[A-Z][a-z]+$/ && $prevWord =~ /^[!\.\?]$/);    ## Initial cap & teh previous word is a dot
		$features .= " PREV_LOWER=$prevWordLower&CUR_LOWER=$wordLower"; ## The previous+current words converted to lowercase

		### 2.5. Output the extracted features
		print OUTPUT "$features $tag\n";
		print OUTPUT_NOLABEL "$features\n"  if (defined $outputNoLabel);

		### 2.6. Set the previous word
		$prevWord = $word;
		$prevWordLower = $wordLower;
	}

	### 3. Close the files
	close(INPUT)  or die "Failed to close $input";
	close(OUTPUT) or die "Failed to close $output";
	close(OUTPUT_NOLABEL) or die "Failed to close $outputNoLabel" if (defined $outputNoLabel);
}
