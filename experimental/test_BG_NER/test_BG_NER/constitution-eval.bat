@echo STEP 1: Extract the features
perl constitution-extract-features.pl

@echo STEP 2: Train a CRF Model
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --train true --model-file constitution-train-feat.model constitution-train-feat.txt

@echo STEP 3: Evaluate the Model
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --test seg=B-PER.I-PER,B-ORG.I-ORG,B-LOC.I-LOC,B-OTH.I-OTH --model-file constitution-train-feat.model constitution-test-feat.txt

@echo STEP 4: Extract the annotations
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --model-file constitution-train-feat.model --include-input true constitution-test-feat-noLabel.txt > constitution-test-feat-noLabel.out
