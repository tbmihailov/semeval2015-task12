@echo STEP 1: Extract the features
perl rest-extract-features.pl

@echo STEP 2: Train a CRF Model
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --orders 1,2 --train true --model-file rest-train-feat-order2.model rest-train-feat.txt

@echo STEP 3: Evaluate the Model
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --orders 1,2 --test seg=RESTAURANT,FOOD,DRINKS,SERVICE,AMBIENCE,LOCATION  --model-file rest-train-feat-order2.model rest-test-feat.txt

@echo STEP 4: Extract the annotations
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --orders 1,2 --model-file rest-train-feat-order2.model --include-input true rest-test-feat-noLabel.txt > rest-test-feat-noLabel.out
