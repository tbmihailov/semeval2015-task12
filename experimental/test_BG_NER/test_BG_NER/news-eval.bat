@echo STEP 1: Extract the features
perl news-extract-features.pl

@echo STEP 2: Train a CRF Model
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --train true --model-file news-train-feat.model news-train-feat.txt

@echo STEP 3: Evaluate the Model
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --test seg=B-PER.I-PER,B-ORG.I-ORG,B-LOC.I-LOC --model-file news-train-feat.model news-test-feat.txt

@echo STEP 4: Extract the annotations
java -Xmx1024m -cp "D:\Software\mallet\mallet-0.4\class;D:\Software\mallet\mallet-0.4\lib\mallet-deps.jar" edu.umass.cs.mallet.base.fst.SimpleTagger --model-file news-train-feat.model --include-input true news-test-feat-noLabel.txt > news-test-feat-noLabel.out
